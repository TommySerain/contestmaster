package co.contestmaster.business;

import co.contestmaster.entity.Party;
import co.contestmaster.entity.Tournament;
import co.contestmaster.entity.User;
import co.contestmaster.repository.interfaces.TournamentRepositoryInterface;
import co.contestmaster.repository.interfaces.UserRepositoryInterface;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PartyBusiness {

  @Autowired private UserRepositoryInterface userRepository;

  @Autowired private TournamentRepositoryInterface tournamentRepository;

  public List<Party> getAllinformations(List<Party> partiesFromDatabase) {
    for (Party party : partiesFromDatabase) {
      try {
        if (party.getUser1() != null && party.getUser1().getId() != null) {
          User user1FromDatabase = userRepository.getById(party.getUser1().getId());
          party.setUser1(user1FromDatabase);
        }

        if (party.getUser2() != null && party.getUser2().getId() != null) {
          User user2FromDatabase = userRepository.getById(party.getUser2().getId());
          party.setUser2(user2FromDatabase);
        }

        if (party.getTournament() != null && party.getTournament().getId() != null) {
          Tournament tournamentFromDatabase =
              tournamentRepository.getById(party.getTournament().getId());
          party.setTournament(tournamentFromDatabase);
        }

        if (party.getWinner() != null && party.getWinner().getId() != null) {
          User winnerFromDatabase = userRepository.getById(party.getWinner().getId());
          party.setWinner(winnerFromDatabase);
        }
      } catch (Exception e) {
      }
    }
    return partiesFromDatabase;
  }

  public Party getInformations(Party partyFromDatabase) {
    try {
      if (partyFromDatabase.getUser1() != null && partyFromDatabase.getUser1().getId() != null) {
        User user1FromDatabase = userRepository.getById(partyFromDatabase.getUser1().getId());
        partyFromDatabase.setUser1(user1FromDatabase);
      }

      if (partyFromDatabase.getUser2() != null && partyFromDatabase.getUser2().getId() != null) {
        User user2FromDatabase = userRepository.getById(partyFromDatabase.getUser2().getId());
        partyFromDatabase.setUser2(user2FromDatabase);
      }

      if (partyFromDatabase.getTournament() != null
          && partyFromDatabase.getTournament().getId() != null) {
        Tournament tournamentFromDatabase =
            tournamentRepository.getById(partyFromDatabase.getTournament().getId());
        partyFromDatabase.setTournament(tournamentFromDatabase);
      }

      if (partyFromDatabase.getWinner() != null && partyFromDatabase.getWinner().getId() != null) {
        User winnerFromDatabase = userRepository.getById(partyFromDatabase.getWinner().getId());
        partyFromDatabase.setWinner(winnerFromDatabase);
      }

    } catch (Exception e) {
    }

    return partyFromDatabase;
  }
}
