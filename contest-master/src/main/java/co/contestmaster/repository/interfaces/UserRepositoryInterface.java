package co.contestmaster.repository.interfaces;

import co.contestmaster.entity.User;
import java.util.List;

public interface UserRepositoryInterface extends CrudRepository<User> {

  List<User> getByCity(String city);

  User getByPseudo(String pseudo);

  User getByUid(String uid);

  List<User> getUsersByTournament(String name);
}
