package co.contestmaster.repository;

import co.contestmaster.entity.UserAvatar;
import co.contestmaster.repository.interfaces.UserAvatarRepositoryInterface;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class UserAvatarRepository extends AbstractRepository<UserAvatar>
    implements UserAvatarRepositoryInterface {
  private List<UserAvatar> userAvatars = new ArrayList<>();

  public UserAvatarRepository() {
    this.addSQL = "INSERT INTO user_avatar (user,avatar) VALUES (?,?)";
    this.getAllSQL = "SELECT * FROM user_avatar";
    this.getByIdSQL = "SELECT * FROM user_avatar WHERE id=?";
    this.updateSQL = "UPDATE user_avatar SET user=?,avatar=? WHERE id=?";
    this.deleteSQL = "DELETE FROM user_avatar WHERE id=?";
  }

  @Override
  protected void setEntityId(UserAvatar userAvatar, int id) {
    userAvatar.setId(id);
  }

  @Override
  protected void entityBindValues(PreparedStatement stmt, UserAvatar userAvatar)
      throws SQLException {
    stmt.setInt(1, userAvatar.getUser().getId());
    stmt.setInt(2, userAvatar.getAvatar().getId());
  }

  @Override
  protected void entityBindValuesWithId(PreparedStatement stmt, UserAvatar userAvatar, Integer id)
      throws SQLException {
    entityBindValues(stmt, userAvatar);
    stmt.setInt(3, id);
  }

  @Override
  protected UserAvatar sqlToEntity(ResultSet rs) throws SQLException {
    Integer id = rs.getInt("id");
    Integer user = rs.getInt("user");
    Integer avatar = rs.getInt("avatar");
    return new UserAvatar(id, user, avatar);
  }
}
