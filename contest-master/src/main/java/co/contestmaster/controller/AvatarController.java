package co.contestmaster.controller;

import co.contestmaster.business.AvatarBusiness;
import co.contestmaster.dto.AddAvatarRequestDto;
import co.contestmaster.entity.Avatar;
import co.contestmaster.exception.AvatarAlreadyAssignedException;
import co.contestmaster.repository.AvatarRepository;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/api/avatar")
public class AvatarController implements AbstractController<Avatar> {

  @Autowired
  private AvatarRepository avatarRepository;

  @Autowired
  private AvatarBusiness avatarBusiness;

  @PostMapping(value = "/add")
  @ResponseStatus(HttpStatus.CREATED)
  public Avatar add(@RequestBody Avatar entity) {
    boolean added = avatarRepository.add(entity);
    if (!added) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    return entity;
  }

  @PatchMapping(value = "/{id}")
  public Avatar update(@RequestBody Avatar entity, @PathVariable Integer id) {
    Avatar avatarToUpdate = getById(id);
    if (entity.getName() != null) {
      avatarToUpdate.setName(entity.getName());
    }
    if (entity.getImage() != null) {
      avatarToUpdate.setImage(entity.getImage());
    }
    if (entity.getCost() != null) {
      avatarToUpdate.setCost(entity.getCost());
    }
    if (entity.getDescription() != null) {
      avatarToUpdate.setDescription(entity.getDescription());
    }
    avatarRepository.update(avatarToUpdate, id);
    return avatarToUpdate;
  }

  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable Integer id) {
    if (!avatarRepository.delete(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(value = { "/", "" })
  public List<Avatar> getAll() {
    return avatarRepository.getAll();
  }

  @GetMapping(value = "/{id}")
  public Avatar getById(@PathVariable Integer id) {
    return avatarRepository.getById(id);
  }

  @GetMapping(value = "/name/{name}")
  public Avatar getByName(@PathVariable String name) {
    return avatarRepository.getByName(name);
  }

  @GetMapping(value = "/avatars/{userId}")
  public List<Avatar> getAvatarsByUserId(@PathVariable String userId) {
    return avatarRepository.getAvatarsByUserId(userId);
  }

  @PostMapping(value = "/addToUser/{userId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void addAvatarToUser(@PathVariable int userId, @RequestBody AddAvatarRequestDto request) {
        try {
            boolean added = avatarBusiness.addAvatarToUser(userId, request.getAvatarId());
            if (!added) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Failed to add avatar to user");
            }
        } catch (AvatarAlreadyAssignedException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
