package co.contestmaster.entity;

public class City {

  private Integer id;
  private String name;
  private String zipcode;

  public City() {}

  public City(Integer id) {
    this.id = id;
  }

  public City(String name, String zipcode) {
    this.name = name;
    this.zipcode = zipcode;
  }

  public City(Integer id, String name, String zipcode) {
    this.id = id;
    this.name = name;
    this.zipcode = zipcode;
  }

  /**
   * @return Integer return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return String return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return String return the zipcode
   */
  public String getZipcode() {
    return zipcode;
  }

  /**
   * @param zipcode the zipcode to set
   */
  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }
}
