package co.contestmaster.repository;

import co.contestmaster.entity.Type;
import co.contestmaster.repository.interfaces.TypeRepositoryInterface;
import java.sql.*;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TypeRepository extends AbstractRepository<Type> implements TypeRepositoryInterface {
  @Autowired private DataSource dataSource;
  private String getByNameSql = "SELECT * FROM type WHERE name=?";

  public TypeRepository() {
    this.addSQL = "INSERT INTO type (name,description) VALUES (?,?)";
    this.getAllSQL = "SELECT * FROM type";
    this.getByIdSQL = "SELECT * FROM type WHERE id=?";
    this.updateSQL = "UPDATE type SET name=?,description=? WHERE id=?";
    this.deleteSQL = "DELETE FROM type WHERE id=?";
  }

  @Override
  protected void setEntityId(Type type, int id) {
    type.setId(id);
  }

  @Override
  protected void entityBindValues(PreparedStatement stmt, Type type) throws SQLException {
    stmt.setString(1, type.getName());
    stmt.setString(2, type.getDescription());
  }

  @Override
  protected void entityBindValuesWithId(PreparedStatement stmt, Type type, Integer id)
      throws SQLException {
    entityBindValues(stmt, type);
    stmt.setInt(3, id);
  }

  @Override
  protected Type sqlToEntity(ResultSet rs) throws SQLException {
    Integer id = rs.getInt("id");
    String name = rs.getString("name");
    String description = rs.getString("description");
    return new Type(id, name, description);
  }

  @Override
  public Type getByName(String name) {
    Type type;
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByNameSql);
      stmt.setString(1, name);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        type = sqlToEntity(rs);
        return type;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
}
