package co.contestmaster.entity;

import java.time.LocalDateTime;

public class Party {
  private Integer id;
  private LocalDateTime date;
  private User winner;
  private User user1;
  private User user2;
  private String round;
  private Tournament tournament;

  public Party() {}

  public Party(
      LocalDateTime date,
      User winner,
      User user_id1,
      User user_id2,
      String round,
      Tournament tournament) {
    this.date = date;
    this.winner = winner;
    this.user1 = user_id1;
    this.user2 = user_id2;
    this.round = round;
    this.tournament = tournament;
  }

  public Party(
      Integer id,
      LocalDateTime date,
      User winner,
      User user_id1,
      User user_id2,
      String round,
      Tournament tournament) {
    this.id = id;
    this.date = date;
    this.winner = winner;
    this.user1 = user_id1;
    this.user2 = user_id2;
    this.round = round;
    this.tournament = tournament;
  }

  public Party(
      Integer id,
      LocalDateTime date,
      Integer winner_id,
      Integer user_id1,
      Integer user_id2,
      String round,
      Integer Tournament_id) {
    this.id = id;
    this.date = date;
    this.winner = new User(winner_id);
    this.user1 = new User(user_id1);
    this.user2 = new User(user_id2);
    this.round = round;
    this.tournament = new Tournament(Tournament_id);
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer party) {
    this.id = party;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  public User getWinner() {
    return winner;
  }

  public void setWinner(User winner) {
    this.winner = winner;
  }

  public User getUser1() {
    return user1;
  }

  public void setUser1(User user_id1) {
    this.user1 = user_id1;
  }

  public User getUser2() {
    return user2;
  }

  public void setUser2(User user_id2) {
    this.user2 = user_id2;
  }

  public String getRound() {
    return round;
  }

  public void setRound(String round) {
    this.round = round;
  }

  public Tournament getTournament() {
    return tournament;
  }

  public void setTournament(Tournament tournament) {
    this.tournament = tournament;
  }
}
