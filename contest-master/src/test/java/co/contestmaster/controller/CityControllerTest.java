package co.contestmaster.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import co.contestmaster.entity.City;
import co.contestmaster.repository.CityRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class CityControllerTest {

  @Autowired MockMvc mvc;
  @Autowired private CityRepository cityRepository;

  @Test
  void testAdd() throws Exception {
    mvc.perform(
            post("/api/city/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    """
                        {
                            "name": "Marseille",
                            "zipcode": "13000"
                        }
                        """))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id").isNumber());
  }

  @Test
  void testGetAll() throws Exception {
    mvc.perform(get("/api/city"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").isArray())
        .andExpect(jsonPath("$[?(@.name == null)]").isEmpty())
        .andExpect(jsonPath("$[?(@.zipcode == null)]").isEmpty());
  }

  @Test
  void testGetById() throws Exception {
    mvc.perform(get("/api/city/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name").value("LYON"))
        .andExpect(jsonPath("$.id").value(1))
        .andExpect(jsonPath("$.zipcode").value("69000"));
  }

  @Test
  void testGetByIdNotFound() throws Exception {
    mvc.perform(get("/api/city/100000")).andExpect(status().isNotFound());
  }

  @Test
  void testUpdate() throws Exception {
    mvc.perform(
            patch("/api/city/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    """
                        {
                            "zipcode": "69000"
                        }
                        """))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name").value("LYON"))
        .andExpect(jsonPath("$.zipcode").value("69000"))
        .andExpect(jsonPath("$.id").value(1));
  }

  @Test
  void testDelete() throws Exception {
    // First, ensure the city exists
    City city = new City("TestCity", "12345");
    cityRepository.add(city);
    Integer cityId = city.getId();

    mvc.perform(delete("/api/city/" + cityId)).andExpect(status().isNoContent());

    // Verify deletion
    mvc.perform(get("/api/city/" + cityId)).andExpect(status().isNotFound());
  }

  @Test
  void testDeleteNotFound() throws Exception {
    mvc.perform(delete("/api/city/100000")).andExpect(status().isNotFound());
  }

  @Test
  void testGetByName() throws Exception {
    mvc.perform(get("/api/city/name/GRENOBLE"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name").value("GRENOBLE"))
        .andExpect(jsonPath("$.id").value(2))
        .andExpect(jsonPath("$.zipcode").value("38000"));
  }

  @Test
  void testGetByNameNotFound() throws Exception {
    mvc.perform(get("/api/city/name/BLOB")).andExpect(status().isNotFound());
  }

  @Test
  void testGetByZipCode() throws Exception {
    mvc.perform(get("/api/city/zipcode/38000"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").isArray())
        .andExpect(jsonPath("$[0].name").value("GRENOBLE"))
        .andExpect(jsonPath("$[?(@.name == null)]").isEmpty())
        .andExpect(jsonPath("$[?(@.zipcode == null)]").isEmpty());
  }

  @Test
  void testGetByZipCodeNotFound() throws Exception {
    mvc.perform(get("/api/city/zipcode/69654"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").isArray())
        .andExpect(jsonPath("$").isEmpty());
  }
}
