package co.contestmaster.repository.interfaces;

import co.contestmaster.entity.Tournament;
import java.time.LocalDateTime;
import java.util.List;

public interface TournamentRepositoryInterface extends CrudRepository<Tournament> {

  List<Tournament> getByType(String type);

  List<Tournament> getByCity(String city);

  List<Tournament> getByPlatform(String platform);

  List<Tournament> getByGame(String game);

  List<Tournament> getByBeginDate(LocalDateTime date);

  List<Tournament> getByEndDate(LocalDateTime date);

  Tournament getByName(String name);

  List<Tournament> getTournamentsByUser(String name);

  Integer getNbPartyNotFinishedByRound(String round, Tournament tournament);

  void adjustParticipants(int tournamentId, int nbParticipants);

  void deleteTournamentBots(int tournamentId);
}
