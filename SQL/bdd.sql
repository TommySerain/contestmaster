CREATE DATABASE contest_master CHARACTER SET 'utf8';

USE contest_master;

CREATE TABLE city(
                    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                    name VARCHAR(255) NOT NULL,
                    zipcode VARCHAR(255)
)
    ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE plateform(
                     id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                     name VARCHAR(255) NOT NULL,
                     description TEXT
)
    ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE type(
                    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                    name VARCHAR(255) NOT NULL,
                    description TEXT
)
    ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE game(
                     id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                     name VARCHAR(255) NOT NULL,
                     description TEXT
)
    ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE tournament(
                           id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                           name VARCHAR(255) NOT NULL,
                           participantNbr INT NOT NULL,
                           beginDate DATETIME NOT NULL,
                           endDate DATETIME NOT NULL,
                           image VARCHAR(255),
                           address VARCHAR(255),
                           city_id INT,
                           type_id INT NOT NULL,
                           reward VARCHAR(255),
                           entry_price FLOAT,
                           description TEXT,
                           plateform_id INT NOT NULL,
                           game_id INT NOT NULL,
                           planner VARCHAR(255),
                           round_type VARCHAR(255),
                           winner VARCHAR(255),
                           FOREIGN KEY (city_id) REFERENCES city(id),
                           FOREIGN KEY (type_id) REFERENCES type(id),
                           FOREIGN KEY (game_id) REFERENCES game(id),
                           FOREIGN KEY (plateform_id) REFERENCES plateform(id)
)
    ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE user(
                    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                    name VARCHAR(255) NOT NULL,
                    pseudo VARCHAR(255) NOT NULL,
                    description TEXT,
                    email VARCHAR(320) NOT NULL,
                    password VARCHAR(80) NOT NULL,
                    role VARCHAR(20) NOT NULL,
                    image VARCHAR(255),
                    score INT,
                    city_id INT NOT NULL,
                    tournament_id INT NOT NULL,
                    game_id INT NOT NULL,
                    plateform_id INT NOT NULL,
                    FOREIGN KEY (city_id) REFERENCES city(id),
                    FOREIGN KEY (tournament_id) REFERENCES tournament(id),
                    FOREIGN KEY (game_id) REFERENCES game(id),
                    FOREIGN KEY (plateform_id) REFERENCES plateform(id)
)
    ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE party(
                     id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                     date DATETIME NOT NULL,
                     round VARCHAR(255) NOT NULL,
                     winner INT,
                     user1 INT NOT NULL,
                     user2 INT NOT NULL,
                     FOREIGN KEY (winner) REFERENCES user(id),
                     FOREIGN KEY (user1) REFERENCES user(id),
                     FOREIGN KEY (user2) REFERENCES user(id)
)
    ENGINE=INNODB DEFAULT CHARSET=utf8;

