package co.contestmaster.entity;

public class User {
  private Integer id;
  private String name;
  private String pseudo = "";
  private String description = "";
  private City city = null;
  private String email = "";
  private String image = null;
  private Tournament tournament;
  private Integer gold = null;
  private Boolean isBot = false;
  private String uid;

  public User() {}

  public User(
      String name,
      String pseudo,
      String description,
      City city,
      String email,
      String image,
      Tournament tournament,
      Integer gold,
      String uid) {
    this.name = name;
    this.pseudo = pseudo;
    this.description = description;
    this.city = city;
    this.email = email;
    this.image = image;
    this.tournament = tournament;
    this.gold = gold;
    this.setIsBot(false);
    this.uid = uid;
  }

  public User(
      String name,
      String pseudo,
      String description,
      City city,
      String email,
      String image,
      Tournament tournament,
      Integer gold,
      Boolean isBot,
      String uid) {
    this.name = name;
    this.pseudo = pseudo;
    this.description = description;
    this.city = city;
    this.email = email;
    this.image = image;
    this.tournament = tournament;
    this.gold = gold;
    this.isBot = isBot;
    this.uid = uid;
  }

  public User(
      Integer id,
      String name,
      String pseudo,
      String description,
      Integer city,
      String email,
      String image,
      Integer tournament,
      Integer gold,
      String uid) {
    this.id = id;
    this.name = name;
    this.pseudo = pseudo;
    this.description = description;
    this.city = new City(city);
    this.email = email;
    this.image = image;
    this.tournament = new Tournament(tournament);
    this.gold = gold;
    this.setIsBot(false);
    this.uid = uid;
  }

  public User(
      Integer id,
      String name,
      String pseudo,
      String description,
      Integer city,
      String email,
      String image,
      Integer tournament,
      Integer gold,
      Boolean isBot,
      String uid) {
    this.id = id;
    this.name = name;
    this.pseudo = pseudo;
    this.description = description;
    this.city = new City(city);
    this.email = email;
    this.image = image;
    this.tournament = new Tournament(tournament);
    this.gold = gold;
    this.isBot = isBot;
    this.uid = uid;
  }

  public User(Integer id) {
    this.id = id;
  }

  public User(String name, String email, String uid) {
    this.name = name;
    this.email = email;
    this.uid = uid;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPseudo() {
    return pseudo;
  }

  public void setPseudo(String pseudo) {
    this.pseudo = pseudo;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public City getCity() {
    return city;
  }

  public void setCity(City city) {
    this.city = city;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public Tournament getTournament() {
    return tournament;
  }

  public void setTournament(Tournament tournament) {
    this.tournament = tournament;
  }

  public Integer getGold() {
    return gold;
  }

  public void setGold(Integer gold) {
    this.gold = gold;
  }

  public Boolean isBot() {
    return isBot;
  }

  public void setIsBot(Boolean isBot) {
    this.isBot = isBot;
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }
}
