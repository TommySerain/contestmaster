INSERT INTO
    city (name, zipcode)
VALUES ("LYON", 69000), ("GRENOBLE", "38000"), ("BOURG-EN-BRESSE", "01000");

INSERT INTO
    platform (name, description)
VALUES (
        "PS5",
        "Console Playstation 5"
    ), (
        "SWITCH",
        "Console NINTENDO SWITCH"
    ), (
        "FOOT_EXT",
        "Terrain de foot extérieur"
    ), (
        "FOOT_INT",
        "Terrain de foot en salle"
    ), (
        "SALLE",
        "EVENEMENT TOURNOI EN SALLE DE JEUX"
    ), (
        "RUGBY_EXT",
        "Terrain de rugby extérieur"
    );

INSERT INTO
    `type` (name, description)
VALUES (
        "SPORT",
        "Tournoi de sport en physique"
    ), (
        "E_SPORT",
        "Tournoi en LAN de jeux vidéos"
    ), (
        "JEUX_SOCIETE",
        "Tournoi de jeux de sociétés"
    );

INSERT INTO
    game (name, description)
VALUES (
        "FIFA",
        "Jeu de foot sur PC, PS5, XBOX"
    ), (
        "NBA2K23",
        "Jeu de basket sur PC, PS5, XBOX"
    ), (
        "CALL OF DUTY",
        "FPS jeu de combat à la première personne ou à la troisième personne"
    ), (
        "MAGIC THE GATHERING",
        "Jeu de cartes en 1v1 sur des combats de magiciens"
    ), (
        "RUGBY",
        "Tournoi de rubgy avec 20 personnes"
    );

INSERT INTO
    tournament (
        name,
        team,
        participantNbr,
        beginDate,
        endDate,
        image,
        address,
        city,
        type,
        reward,
        entry_price,
        description,
        platform,
        game,
        planner,
        round_type,
        isPublic
    )
VALUES (
        "Le Tournoi des magiciens",
        2,
        30,
        "2024-10-26 14:30:00",
        "2024-10-26 19:30:00",
        "magicien.png",
        "104 rue des mages",
        1,
        3,
        "Jeu de société gratuit",
        10.5,
        "Un tournoi de participants en ronde suisse",
        5,
        4,
        null,
        "POULES",
        1
    ), (
        "FGS",
        1,
        32,
        "2024-06-10 13:30:00",
        "2024-06-10 20:30:00",
        "fifa.png",
        "1 rue de lafayette",
        1,
        2,
        "Jeu FIFA",
        20,
        "Un tournoi de FIFA avec poules",
        1,
        1,
        null,
        "POULES",
        1
    ), (
        "Le Tournoi des champions de rugby",
        2,
        45,
        "2024-01-20 08:30:00",
        "2024-01-20 20:00:00",
        "rugby.png",
        "32 impasse des cours",
        2,
        1,
        "200 euros",
        14.5,
        "Un tournoi éliminatoire entre 3 équipes de 15",
        6,
        5,
        null,
        "ELIMINATOIRES",
        0
    );

INSERT INTO
    user (
        name,
        pseudo,
        description,
        email,
        image,
        gold,
        isBot,
        city,
        tournament,
        uid
    )
VALUES (
        "Sebastien",
        "Syllas",
        "Solo",
        "sebastien.pelc@gmail.com",
        "seb.png",
        0,
        false,
        null,
        1,
        "Eidej9832832jdeoj0923"
    ), (
        "Paul",
        "Joker",
        "Solo",
        "paul.klein@gmail.com",
        "paul.png",
        0,
        false,
        1,
        2,
        "Eidej9832832jdeoj32323"
    ), (
        "Tommy",
        "tom le boss",
        "Equipe rugby : liste participants...",
        "tommy.serain@gmail.com",
        "tommy.png",
        0,
        false,
        2,
        3,
        "CZOxujTb6FbDAQsyFLqlxhdMhtb2"
    ), (
        "Walid",
        "test match",
        "...",
        "walid@gmail.com",
        "walid.png",
        0,
        false,
        3,
        1,
        "Eidej9832832jdeoj0923sk0139jded"
    );

INSERT INTO
    user_tournament (user, tournament)
VALUES (1, 1), (4, 1);

INSERT INTO
    party (
        date,
        round,
        winner,
        user1,
        user2,
        tournament
    )
VALUES (
        "2024-10-26 15:00:00",
        "Demi_finale",
        null,
        1,
        4,
        1
    );
INSERT INTO avatar(
                   name,
                   image,
                   cost,
                   description
)
VALUES (
        'avatar1',
        'avatar1.jpg',
        125,
        'Découvrez avatar 1'
       ),
    (
        'avatar2',
        'avatar2.jpg',
        300,
        'Découvrez avatar 2'
    );

INSERT INTO
    user_avatar (user, avatar)
VALUES (1, 1), (2, 2);

UPDATE tournament SET planner = 1 WHERE id = 1;

UPDATE tournament SET planner = 2 WHERE id = 2;

UPDATE tournament SET planner = 3 WHERE id = 3;