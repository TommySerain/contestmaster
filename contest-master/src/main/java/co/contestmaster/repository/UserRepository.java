package co.contestmaster.repository;

import co.contestmaster.entity.*;
import co.contestmaster.repository.interfaces.UserRepositoryInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository extends AbstractRepository<User> implements UserRepositoryInterface {
  @Autowired private DataSource dataSource;

  private String getByCitySQL =
      "SELECT * FROM user INNER JOIN city ON user.city = city.id WHERE city.name=?";
  private String getByPseudoSQL = "SELECT * FROM user WHERE pseudo=?";
  private String getByUidSQL = "SELECT * FROM user WHERE uid=?";
  private String getUsersByTournament =
      "SELECT * FROM user INNER JOIN user_tournament ON user.id = user_tournament.user INNER JOIN tournament ON tournament.id = user_tournament.tournament WHERE tournament.name LIKE ?";

  public UserRepository() {
    this.addSQL =
        "INSERT INTO user (name,pseudo,description,city,email,image,tournament,gold,isBot,uid) "
            + "VALUES (?,?,?,?,?,?,?,?,?,?)";
    this.getAllSQL = "SELECT * FROM user";
    this.getByIdSQL = "SELECT * FROM user WHERE id=?";
    this.updateSQL =
        "UPDATE user SET name=?,pseudo=?,description=?,city=?,email=?,image=?,tournament=?,gold=?, isBot=?, uid=?"
            + " WHERE id=?";
    this.deleteSQL = "DELETE FROM user WHERE id=?";
  }

  @Override
  protected void setEntityId(User user, int id) {
    user.setId(id);
  }

  @Override
  protected void entityBindValues(PreparedStatement stmt, User user) throws SQLException {
    stmt.setString(1, user.getName());
    stmt.setString(2, user.getPseudo());
    stmt.setString(3, user.getDescription());

    // Vérification si city est null
    if (user.getCity() != null) {
      stmt.setInt(4, user.getCity().getId());
    } else {
      stmt.setNull(4, java.sql.Types.INTEGER); // Si city est null, on met NULL dans la base
    }

    stmt.setString(5, user.getEmail());
    stmt.setString(6, user.getImage());

    // Vérification si tournament est null
    if (user.getTournament() != null) {
      stmt.setInt(7, user.getTournament().getId());
    } else {
      stmt.setNull(7, java.sql.Types.INTEGER); // Si tournament est null, on met NULL dans la base
    }

    // Vérification si gold est null
    if (user.getGold() != null) {
      stmt.setInt(8, user.getGold());
    } else {
      stmt.setNull(8, java.sql.Types.INTEGER); // Si gold est null, on met NULL dans la base
    }

    stmt.setBoolean(9, user.isBot());
    stmt.setString(10, user.getUid());
  }

  @Override
  protected void entityBindValuesWithId(PreparedStatement stmt, User user, Integer id)
      throws SQLException {
    entityBindValues(stmt, user);
    stmt.setInt(10, id);
  }

  @Override
  public User sqlToEntity(ResultSet rs) throws SQLException {
    try {
      Integer id = rs.getInt("id");
      String name = rs.getString("name");
      String pseudo = rs.getString("pseudo");
      String description = rs.getString("description");
      Integer city = rs.getInt("city");
      String email = rs.getString("email");
      String image = rs.getString("image");
      Integer tournament = rs.getInt("tournament");
      Integer gold = rs.getInt("gold");
      Boolean isBot = rs.getBoolean("isBot");
      String uid = rs.getString("uid");
      return new User(
          id, name, pseudo, description, city, email, image, tournament, gold, isBot, uid);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    }
  }

  @Override
  public List<User> getByCity(String city) {
    List<User> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByCitySQL);
      stmt.setString(1, city);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        list.add(sqlToEntity(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public User getByPseudo(String pseudo) {
    User user;
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByPseudoSQL);
      stmt.setString(1, pseudo);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        user = sqlToEntity(rs);
        return user;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public User getByUid(String uid) {
    User user;
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByUidSQL);
      stmt.setString(1, uid);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        user = sqlToEntity(rs);
        return user;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public List<User> getUsersByTournament(String name) {
    List<User> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      String query = "%" + name + "%";
      PreparedStatement stmt = connection.prepareStatement(this.getUsersByTournament);
      stmt.setString(1, query);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        list.add(sqlToEntity(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }
}
