package co.contestmaster.repository;

import co.contestmaster.entity.Platform;
import co.contestmaster.repository.interfaces.PlatformRepositoryInterface;
import java.sql.*;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PlatformRepository extends AbstractRepository<Platform>
    implements PlatformRepositoryInterface {
  @Autowired private DataSource dataSource;
  private String getByNameSql = "SELECT * FROM platform WHERE name=?";

  public PlatformRepository() {
    this.addSQL = "INSERT INTO platform (name,description) VALUES (?,?)";
    this.getAllSQL = "SELECT * FROM platform";
    this.getByIdSQL = "SELECT * FROM platform WHERE id=?";
    this.updateSQL = "UPDATE platform SET name=?,description=? WHERE id=?";
    this.deleteSQL = "DELETE FROM platform WHERE id=?";
  }

  @Override
  protected void setEntityId(Platform platform, int id) {
    platform.setId(id);
  }

  @Override
  protected void entityBindValues(PreparedStatement stmt, Platform platform) throws SQLException {
    stmt.setString(1, platform.getName());
    stmt.setString(2, platform.getDescription());
  }

  @Override
  protected void entityBindValuesWithId(PreparedStatement stmt, Platform platform, Integer id)
      throws SQLException {
    entityBindValues(stmt, platform);
    stmt.setInt(3, id);
  }

  @Override
  protected Platform sqlToEntity(ResultSet rs) throws SQLException {
    Integer id = rs.getInt("id");
    String name = rs.getString("name");
    String description = rs.getString("description");
    return new Platform(id, name, description);
  }

  public Platform getByName(String name) {
    Platform platform;
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByNameSql);
      stmt.setString(1, name);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        platform = sqlToEntity(rs);
        return platform;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
}
