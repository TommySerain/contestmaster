package co.contestmaster.exception;

public class AvatarAlreadyAssignedException extends RuntimeException {
    public AvatarAlreadyAssignedException(String message) {
        super(message);
    }
}