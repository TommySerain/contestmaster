package co.contestmaster.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import co.contestmaster.entity.Game;
import java.io.IOException;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GameRepositoryTest {

  @Autowired GameRepository gameRepository;

  @Test
  void testGetByNameSuccess() {
    Game game = gameRepository.getByName("FIFA");

    assertEquals(1, game.getId());
    assertEquals("FIFA", game.getName());
  }

  @Test
  void testGetByNameFailed() throws JSONException, IOException {
    Game game = gameRepository.getByName("BLOB");
    assertNull(game);
  }
}
