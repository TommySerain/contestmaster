package co.contestmaster.controller;

import java.util.List;

public abstract interface AbstractController<T> {
  public abstract T add(T entity);

  public abstract T update(T entity, Integer id);

  public abstract void delete(Integer id);

  public abstract List<T> getAll();

  public abstract T getById(Integer id);
}
