package co.contestmaster.repository;

import co.contestmaster.entity.Party;
import co.contestmaster.repository.interfaces.PartyRepositoryInterface;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PartyRepository extends AbstractRepository<Party> implements PartyRepositoryInterface {
  @Autowired private DataSource dataSource;
  private String getByRoundByTournamentSql = "SELECT * FROM party WHERE round=? AND tournament=?";
  private String getPartiesByTournamentSql = "SELECT * FROM party WHERE party.tournament=?";

  public PartyRepository() {
    this.addSQL =
        "INSERT INTO party (date,winner,user1,user2,round, tournament) VALUES (?,?,?,?,?,?)";
    this.getAllSQL = "SELECT * FROM party";
    this.getByIdSQL = "SELECT * FROM party WHERE id=?";
    this.updateSQL =
        "UPDATE party SET date=?,winner=?,user1=?,user2=?,round=?,tournament=? WHERE id=?";
    this.deleteSQL = "DELETE FROM party WHERE id=?";
  }

  @Override
  protected void setEntityId(Party party, int id) {
    party.setId(id);
  }

  @Override
  protected void entityBindValues(PreparedStatement stmt, Party party) throws SQLException {
    //        stmt.setDate(1, java.sql.Date.valueOf(party.getDate()));
    stmt.setTimestamp(1, java.sql.Timestamp.valueOf(party.getDate()));
    if (party.getWinner() == null) {
      stmt.setNull(2, 0);
    } else {
      stmt.setInt(2, party.getWinner().getId());
    }
    stmt.setInt(3, party.getUser1().getId());
    stmt.setInt(4, party.getUser2().getId());
    stmt.setString(5, party.getRound());
    stmt.setInt(6, party.getTournament().getId());
  }

  @Override
  protected void entityBindValuesWithId(PreparedStatement stmt, Party party, Integer id)
      throws SQLException {
    entityBindValues(stmt, party);
    stmt.setInt(7, id);
  }

  @Override
  protected Party sqlToEntity(ResultSet rs) throws SQLException {
    Integer id = rs.getInt("id");
    //        LocalDate date = rs.getDate("date").toLocalDate();
    java.sql.Timestamp timestamp = rs.getTimestamp("date");
    LocalDateTime date = timestamp.toLocalDateTime();
    Integer winner = rs.getInt("winner");
    Integer user1 = rs.getInt("user1");
    Integer user2 = rs.getInt("user2");
    String round = rs.getString("round");
    Integer tournament = rs.getInt("tournament");
    return new Party(id, date, winner, user1, user2, round, tournament);
  }

  @Override
  public List<Party> getByRoundByTournament(String round, int tournamentId) {
    List<Party> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByRoundByTournamentSql);
      stmt.setString(1, round);
      stmt.setInt(2, tournamentId);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        list.add(sqlToEntity(rs));
      }

    } catch (SQLException e) {

      e.printStackTrace();
    }
    return list;
  }

  @Override
  public List<Party> getPartiesByTournament(int id) {
    List<Party> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getPartiesByTournamentSql);
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        list.add(sqlToEntity(rs));
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }
}
