package co.contestmaster.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import co.contestmaster.entity.Tournament;
import java.time.LocalDateTime;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TournamentRepositoryTest {

  @Autowired TournamentRepository tournamentRepository;

  @Test
  void testGetByBeginDateSuccess() {
    LocalDateTime date = LocalDateTime.of(2024, 10, 26, 14, 30, 00);
    LocalDateTime expectDateEndDate = LocalDateTime.of(2024, 10, 26, 19, 30, 00);
    ArrayList<Tournament> tournaments =
        new ArrayList<Tournament>(tournamentRepository.getByBeginDate(date));

    assertEquals(1, tournaments.size());
    assertEquals(1, tournaments.get(0).getId());
    assertEquals("Le Tournoi des magiciens", tournaments.get(0).getName());
    assertEquals(30, tournaments.get(0).getParticipantNbr());
    assertEquals(expectDateEndDate, tournaments.get(0).getEndDate());
    assertEquals("104 rue des mages", tournaments.get(0).getAddress());
    assertEquals("Jeu de société gratuit", tournaments.get(0).getReward());
  }

  @Test
  void testGetByBeginDateFailed() {
    LocalDateTime date = LocalDateTime.of(2030, 10, 26, 14, 30, 00);
    ArrayList<Tournament> tournaments =
        new ArrayList<Tournament>(tournamentRepository.getByBeginDate(date));
    ArrayList<Tournament> expectedList = new ArrayList<Tournament>();
    assertEquals(expectedList, tournaments);
  }

  @Test
  void testGetByCitySuccess() {
    LocalDateTime expectedBeginDate = LocalDateTime.of(2024, 10, 26, 14, 30, 00);
    LocalDateTime expectDateEndDate = LocalDateTime.of(2024, 10, 26, 19, 30, 00);
    ArrayList<Tournament> tournaments =
        new ArrayList<Tournament>(tournamentRepository.getByCity("LYON"));

    assertEquals(2, tournaments.size());
    assertEquals(1, tournaments.get(0).getId());
    assertEquals("Le Tournoi des magiciens", tournaments.get(0).getName());
    assertEquals(30, tournaments.get(0).getParticipantNbr());
    assertEquals(expectedBeginDate, tournaments.get(0).getBeginDate());
    assertEquals(expectDateEndDate, tournaments.get(0).getEndDate());
    assertEquals("104 rue des mages", tournaments.get(0).getAddress());
    assertEquals("Jeu de société gratuit", tournaments.get(0).getReward());
  }

  @Test
  void testGetByCityFailed() {
    ArrayList<Tournament> tournaments =
        new ArrayList<Tournament>(tournamentRepository.getByCity("BLOB"));
    ArrayList<Tournament> expectedList = new ArrayList<Tournament>();
    assertEquals(expectedList, tournaments);
  }

  @Test
  void testGetByEndDateSuccess() {
    LocalDateTime expectDateBeginDate = LocalDateTime.of(2024, 10, 26, 14, 30, 00);
    LocalDateTime date = LocalDateTime.of(2024, 10, 26, 19, 30, 00);
    ArrayList<Tournament> tournaments =
        new ArrayList<Tournament>(tournamentRepository.getByEndDate(date));

    assertEquals(1, tournaments.size());
    assertEquals(1, tournaments.get(0).getId());
    assertEquals("Le Tournoi des magiciens", tournaments.get(0).getName());
    assertEquals(30, tournaments.get(0).getParticipantNbr());
    assertEquals(expectDateBeginDate, tournaments.get(0).getBeginDate());
    assertEquals("104 rue des mages", tournaments.get(0).getAddress());
    assertEquals("Jeu de société gratuit", tournaments.get(0).getReward());
  }

  @Test
  void testGetByEndDateFailed() {
    LocalDateTime date = LocalDateTime.of(2030, 10, 26, 14, 30, 00);
    ArrayList<Tournament> tournaments =
        new ArrayList<Tournament>(tournamentRepository.getByEndDate(date));
    ArrayList<Tournament> expectedList = new ArrayList<Tournament>();
    assertEquals(expectedList, tournaments);
  }

  @Test
  void testGetByGameSuccess() {
    LocalDateTime expectedBeginDate = LocalDateTime.of(2024, 10, 26, 14, 30, 00);
    LocalDateTime expectDateEndDate = LocalDateTime.of(2024, 10, 26, 19, 30, 00);
    ArrayList<Tournament> tournaments =
        new ArrayList<Tournament>(tournamentRepository.getByGame("MAGIC THE GATHERING"));

    assertEquals(1, tournaments.size());
    assertEquals(1, tournaments.get(0).getId());
    assertEquals("Le Tournoi des magiciens", tournaments.get(0).getName());
    assertEquals(30, tournaments.get(0).getParticipantNbr());
    assertEquals(expectedBeginDate, tournaments.get(0).getBeginDate());
    assertEquals(expectDateEndDate, tournaments.get(0).getEndDate());
    assertEquals("104 rue des mages", tournaments.get(0).getAddress());
    assertEquals("Jeu de société gratuit", tournaments.get(0).getReward());
  }

  @Test
  void testGetByGameFailed() {
    ArrayList<Tournament> tournaments =
        new ArrayList<Tournament>(tournamentRepository.getByGame("BLOB"));
    ArrayList<Tournament> expectedList = new ArrayList<Tournament>();
    assertEquals(expectedList, tournaments);
  }

  @Test
  void testGetByName() {
    LocalDateTime expectedBeginDate = LocalDateTime.of(2024, 10, 26, 14, 30, 00);
    LocalDateTime expectDateEndDate = LocalDateTime.of(2024, 10, 26, 19, 30, 00);
    Tournament tournament = tournamentRepository.getByName("Le Tournoi des magiciens");

    assertEquals(1, tournament.getId());
    assertEquals("Le Tournoi des magiciens", tournament.getName());
    assertEquals(30, tournament.getParticipantNbr());
    assertEquals(expectedBeginDate, tournament.getBeginDate());
    assertEquals(expectDateEndDate, tournament.getEndDate());
    assertEquals("104 rue des mages", tournament.getAddress());
    assertEquals("Jeu de société gratuit", tournament.getReward());
  }

  @Test
  void testGetByPlatform() {
    LocalDateTime expectedBeginDate = LocalDateTime.of(2024, 10, 26, 14, 30, 00);
    LocalDateTime expectDateEndDate = LocalDateTime.of(2024, 10, 26, 19, 30, 00);
    ArrayList<Tournament> tournaments =
        new ArrayList<Tournament>(tournamentRepository.getByPlatform("SALLE"));

    assertEquals(1, tournaments.size());
    assertEquals(1, tournaments.get(0).getId());
    assertEquals("Le Tournoi des magiciens", tournaments.get(0).getName());
    assertEquals(30, tournaments.get(0).getParticipantNbr());
    assertEquals(expectedBeginDate, tournaments.get(0).getBeginDate());
    assertEquals(expectDateEndDate, tournaments.get(0).getEndDate());
    assertEquals("104 rue des mages", tournaments.get(0).getAddress());
    assertEquals("Jeu de société gratuit", tournaments.get(0).getReward());
  }

  @Test
  void testGetByType() {
    LocalDateTime expectedBeginDate = LocalDateTime.of(2024, 10, 26, 14, 30, 00);
    LocalDateTime expectDateEndDate = LocalDateTime.of(2024, 10, 26, 19, 30, 00);
    ArrayList<Tournament> tournaments =
        new ArrayList<Tournament>(tournamentRepository.getByType("JEUX_SOCIETE"));

    assertEquals(1, tournaments.size());
    assertEquals(1, tournaments.get(0).getId());
    assertEquals("Le Tournoi des magiciens", tournaments.get(0).getName());
    assertEquals(30, tournaments.get(0).getParticipantNbr());
    assertEquals(expectedBeginDate, tournaments.get(0).getBeginDate());
    assertEquals(expectDateEndDate, tournaments.get(0).getEndDate());
    assertEquals("104 rue des mages", tournaments.get(0).getAddress());
    assertEquals("Jeu de société gratuit", tournaments.get(0).getReward());
  }
}
