package co.contestmaster.business;

import co.contestmaster.entity.City;
import co.contestmaster.entity.Game;
import co.contestmaster.entity.Party;
import co.contestmaster.entity.Platform;
import co.contestmaster.entity.Tournament;
import co.contestmaster.entity.Type;
import co.contestmaster.entity.User;
import co.contestmaster.repository.CityRepository;
import co.contestmaster.repository.TypeRepository;
import co.contestmaster.repository.UserRepository;
import co.contestmaster.repository.interfaces.GameRepositoryInterface;
import co.contestmaster.repository.interfaces.PartyRepositoryInterface;
import co.contestmaster.repository.interfaces.PlatformRepositoryInterface;
import co.contestmaster.repository.interfaces.TournamentRepositoryInterface;
import java.util.List;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TournamentBusiness {

  @Autowired TournamentRepositoryInterface tournamentRepository;

  @Autowired PartyRepositoryInterface partyRepository;
  @Autowired PlatformRepositoryInterface platformRepository;

  @Autowired GameRepositoryInterface gameRepository;

  @Autowired UserRepository userRepository;
  @Autowired TypeRepository typeRepository;

  @Autowired private CityRepository cityRepository;

  private List<User> participants;
  private List<User> bots;

  // On a créé un tableau de winner dans tournament ou on stockera les winners des
  // différents match
  public void createGrid(Tournament tournament) {

    // On définie le nombre de participants,
    // Si on est au premier tour du tournoi, il n'y a pas de gagnant de parties donc on utilisera le
    // compte des inscrits au tournoi.
    // Si on a déjà fait un tour on récupère le nombre de gagnant du tour précédent.

    int nbParticipants;
    if (tournament.getPartiesWinners().isEmpty()) {
      nbParticipants = userRepository.getUsersByTournament(tournament.getName()).size();
    } else {
      nbParticipants = tournament.getPartiesWinners().size();
    }

    // On utilise la méthode du répo qui execute une procédure stocké pour vérifier si le nombre de
    // partcipant est puissance 2
    // Et le cas échéant d'ajouter le nombre de bot voulue
    tournamentRepository.adjustParticipants(tournament.getId(), nbParticipants);

    int nbRound = nbParticipants / 2;

    // On récupère les participants
    if (tournament.getPartiesWinners().isEmpty()) {
      participants = userRepository.getUsersByTournament(tournament.getName());
    } else {
      participants = tournament.getPartiesWinners();
    }
    // On sépare les participants des bots
    for (User participant : participants) {
      if (participant.isBot() == true) {
        bots.add(participant);
        participants.remove(participant);
      }
    }

    for (int i = 0; i < nbRound; i++) {
      Party party = new Party();
      setRoundByNbRound(party, nbRound, i);

      party.setTournament(tournament);

      User user1 = setRandomPlayerToParty(participants);
      User user2;
      party.setUser1(user1);
      participants.remove(user1);

      // Si le tableau de bot n'est pas vide, on définie un bot comme user2 sinon on ajoute un
      // participant.
      if (!bots.isEmpty()) {
        user2 = setRandomPlayerToParty(bots);
        // Un trigger SQL se chargera de définir le user1 comme vainqueur du match.
        // Ici on l'ajoute dans le tableau partiesWinner qui n'est pas une propriété de l'entité.
        tournament.addPartiesWinners(user1);
        bots.remove(user2);
      } else {
        user2 = setRandomPlayerToParty(participants);
        participants.remove(user2);
      }
      party.setUser2(user2);
    }
    // On vide la liste de gagnants des parties précédentes pour pouvoir stocker les gagnants du
    // prochain tour.
    tournament.getPartiesWinners().clear();
  }

  private User setRandomPlayerToParty(List<User> participants) {
    Random random_method = new Random();
    int index = random_method.nextInt(participants.size());
    return participants.get(index);
  }

  private void setRoundByNbRound(Party party, int nbRound, int i) {
    if (nbRound > 4) {
      party.setRound(nbRound + "ème_de_finale" + i);
    }
    if (nbRound == 4) {
      party.setRound("Quart_de_finale" + i);
    }
    if (nbRound == 2) {
      party.setRound("Demi_finale" + i);
    }
    if (nbRound == 1) {
      party.setRound("Finale");
    }
  }

  public boolean validateTournamentRound(String tournamentRound, Tournament tournament) {

    // On vérifie si tous les matchs sont finis
    if (tournamentRepository.getNbPartyNotFinishedByRound(tournamentRound, tournament) != 0) {
      return false;
    }
    List<Party> partiesRound =
        partyRepository.getByRoundByTournament(tournamentRound, tournament.getId());
    for (Party party : partiesRound) {
      tournament.addPartiesWinners(party.getWinner());
    }
    // On valide le round et on crée la suite de la grille si le round actuel n'était pas la finale,
    // Sinon on désigne le vainqueur du tournoi.
    if (tournament.getPartiesWinners().size() > 1) {
      createGrid(tournament);
      // Une fois le round validé, on supprime les bots éventuelement créés
      tournamentRepository.deleteTournamentBots(tournament.getId());
      return true;
    }
    tournament.setWinner(tournament.getPartiesWinners().get(0));
    // Une fois le round validé, on supprime les bots éventuelement créés
    tournamentRepository.deleteTournamentBots(tournament.getId());
    return true;
  }

  public List<Tournament> getAllinformations(List<Tournament> tournamentsFromDatabase) {
    for (Tournament tournament : tournamentsFromDatabase) {
      try {
        if (tournament.getCity() != null && tournament.getCity().getId() != null) {
          City cityFromDatabase = cityRepository.getById(tournament.getCity().getId());
          tournament.setCity(cityFromDatabase);
        }

        if (tournament.getType_id() != null && tournament.getType_id().getId() != null) {
          Type typeFromDatabase = typeRepository.getById(tournament.getType_id().getId());
          tournament.setType_id(typeFromDatabase);
        }

        if (tournament.getPlatform() != null && tournament.getPlatform().getId() != null) {
          Platform platformFromDatabase =
              platformRepository.getById(tournament.getPlatform().getId());
          tournament.setPlatform(platformFromDatabase);
        }

        if (tournament.getGame() != null && tournament.getGame().getId() != null) {
          Game gameFromDatabase = gameRepository.getById(tournament.getGame().getId());
          tournament.setGame(gameFromDatabase);
        }

        if (tournament.getPlanner() != null && tournament.getPlanner().getId() != null) {
          User plannerFromDatabase = userRepository.getById(tournament.getPlanner().getId());
          tournament.setPlanner(plannerFromDatabase);
        }
      } catch (Exception e) {
      }
    }
    return tournamentsFromDatabase;
  }

  public Tournament getInformations(Tournament tournamentFromDatabase) {
    try {
      if (tournamentFromDatabase.getCity() != null
          && tournamentFromDatabase.getCity().getId() != null) {
        City cityFromDatabase = cityRepository.getById(tournamentFromDatabase.getCity().getId());
        tournamentFromDatabase.setCity(cityFromDatabase);
      }

      if (tournamentFromDatabase.getType_id() != null
          && tournamentFromDatabase.getType_id().getId() != null) {
        Type typeFromDatabase = typeRepository.getById(tournamentFromDatabase.getType_id().getId());
        tournamentFromDatabase.setType_id(typeFromDatabase);
      }

      if (tournamentFromDatabase.getPlatform() != null
          && tournamentFromDatabase.getPlatform().getId() != null) {
        Platform platformFromDatabase =
            platformRepository.getById(tournamentFromDatabase.getPlatform().getId());
        tournamentFromDatabase.setPlatform(platformFromDatabase);
      }

      if (tournamentFromDatabase.getGame() != null
          && tournamentFromDatabase.getGame().getId() != null) {
        Game gameFromDatabase = gameRepository.getById(tournamentFromDatabase.getGame().getId());
        tournamentFromDatabase.setGame(gameFromDatabase);
      }

      if (tournamentFromDatabase.getPlanner() != null
          && tournamentFromDatabase.getPlanner().getId() != null) {
        User plannerFromDatabase =
            userRepository.getById(tournamentFromDatabase.getPlanner().getId());
        tournamentFromDatabase.setPlanner(plannerFromDatabase);
      }

    } catch (Exception e) {
    }

    return tournamentFromDatabase;
  }
}
