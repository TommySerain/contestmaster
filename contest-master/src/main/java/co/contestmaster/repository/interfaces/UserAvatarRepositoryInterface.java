package co.contestmaster.repository.interfaces;

import co.contestmaster.entity.UserAvatar;

public interface UserAvatarRepositoryInterface extends CrudRepository<UserAvatar> {
}
