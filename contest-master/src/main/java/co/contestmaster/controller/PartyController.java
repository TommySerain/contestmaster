package co.contestmaster.controller;

import co.contestmaster.business.PartyBusiness;
import co.contestmaster.entity.Party;
import co.contestmaster.repository.PartyRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/api/party")
public class PartyController implements AbstractController<Party> {
  @Autowired private PartyRepository partyRepository;
  @Autowired private PartyBusiness partyBusiness;

  @PostMapping(value = "/add")
  @ResponseStatus(HttpStatus.CREATED)
  public Party add(@RequestBody Party entity) {
    boolean added = partyRepository.add(entity);
    if (!added) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    return entity;
  }

  @PatchMapping(value = {"/{id}"})
  public Party update(@RequestBody Party entity, @PathVariable Integer id) {
    Party partyToUpdate = getById(id);
    if (entity.getDate() != null) {
      partyToUpdate.setDate(entity.getDate());
    }
    if (entity.getWinner() != null) {
      partyToUpdate.setWinner(entity.getWinner());
    }
    if (entity.getUser1() != null) {
      partyToUpdate.setUser1(entity.getUser1());
    }
    if (entity.getUser2() != null) {
      partyToUpdate.setUser2(entity.getUser2());
    }
    if (entity.getRound() != null) {
      partyToUpdate.setRound(entity.getRound());
    }
    if (entity.getTournament() != null) {
      partyToUpdate.setTournament(entity.getTournament());
    }
    partyRepository.update(partyToUpdate, id);
    return partyToUpdate;
  }

  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable Integer id) {
    if (!partyRepository.delete(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(value = {"/", ""})
  public List<Party> getAll() {
    List<Party> partiesFromDatabase = partyRepository.getAll();
    partiesFromDatabase = partyBusiness.getAllinformations(partiesFromDatabase);
    return partiesFromDatabase;
  }

  @GetMapping(value = "/{id}")
  public Party getById(@PathVariable Integer id) {
    Party partyFromDatabase = partyRepository.getById(id);
    partyFromDatabase = partyBusiness.getInformations(partyFromDatabase);
    return partyFromDatabase;
  }

  @GetMapping(value = "/round/{round}/{id}")
  public List<Party> getByRoundByTournament(@PathVariable String round, @PathVariable int id) {
    List<Party> partiesFromDatabase = partyRepository.getByRoundByTournament(round, id);
    partiesFromDatabase = partyBusiness.getAllinformations(partiesFromDatabase);
    return partiesFromDatabase;
  }

  @GetMapping(value = "/round/{id}")
  public List<Party> getPartiesByTournament(@PathVariable int id) {
    List<Party> partiesFromDatabase = partyRepository.getPartiesByTournament(id);
    partiesFromDatabase = partyBusiness.getAllinformations(partiesFromDatabase);
    return partiesFromDatabase;
  }
}
