package co.contestmaster.business;

import co.contestmaster.entity.City;
import co.contestmaster.entity.Tournament;
import co.contestmaster.entity.User;
import co.contestmaster.repository.interfaces.CityRepositoryInterface;
import co.contestmaster.repository.interfaces.TournamentRepositoryInterface;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserBusiness {

  @Autowired private CityRepositoryInterface cityRepository;

  @Autowired private TournamentRepositoryInterface tournamentRepository;

  public List<User> getAllinformations(List<User> usersFromDatabase) {
    for (User user : usersFromDatabase) {
      try {
        if (user.getCity() != null && user.getCity().getId() != null) {
          City cityFromDatabase = cityRepository.getById(user.getCity().getId());
          user.setCity(cityFromDatabase);
        }

        if (user.getTournament() != null && user.getTournament().getId() != null) {
          Tournament tournamentFromDatabase =
              tournamentRepository.getById(user.getTournament().getId());
          user.setTournament(tournamentFromDatabase);
        }

      } catch (Exception e) {
      }
    }
    return usersFromDatabase;
  }

  public User getInformations(User userFromDatabase) {

    if (userFromDatabase.getCity() != null && userFromDatabase.getCity().getId() != null) {
      City cityFromDatabase = cityRepository.getById(userFromDatabase.getCity().getId());
      userFromDatabase.setCity(cityFromDatabase);
    }

    if (userFromDatabase.getTournament() != null
        && userFromDatabase.getTournament().getId() != null) {
      Tournament tournamentFromDatabase =
          tournamentRepository.getById(userFromDatabase.getTournament().getId());
      userFromDatabase.setTournament(tournamentFromDatabase);
    }

    return userFromDatabase;
  }
}
