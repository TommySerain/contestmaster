package co.contestmaster.repository.interfaces;

import co.contestmaster.entity.City;
import java.util.List;

public interface CityRepositoryInterface extends CrudRepository<City> {
  List<City> getByZipCode(String zipcode);

  City getByName(String name);
}
