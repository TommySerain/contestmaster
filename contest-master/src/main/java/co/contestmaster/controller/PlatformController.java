package co.contestmaster.controller;

import co.contestmaster.entity.Platform;
import co.contestmaster.repository.PlatformRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/api/platform")
public class PlatformController implements AbstractController<Platform> {
  @Autowired private PlatformRepository platformRepository;

  @PostMapping(value = "/add")
  @ResponseStatus(HttpStatus.CREATED)
  public Platform add(@RequestBody Platform entity) {
    boolean added = platformRepository.add(entity);
    if (!added) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    return entity;
  }

  @PatchMapping(value = "/{id}")
  public Platform update(@RequestBody Platform entity, @PathVariable Integer id) {
    Platform platformToUpdate = getById(id);
    if (entity.getName() != null) {
      platformToUpdate.setName(entity.getName());
    }
    if (entity.getDescription() != null) {
      platformToUpdate.setDescription(entity.getDescription());
    }
    platformRepository.update(platformToUpdate, id);
    return platformToUpdate;
  }

  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable Integer id) {
    if (!platformRepository.delete(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(value = {"/", ""})
  public List<Platform> getAll() {
    return platformRepository.getAll();
  }

  @GetMapping(value = "/{id}")
  public Platform getById(@PathVariable Integer id) {
    return platformRepository.getById(id);
  }

  @GetMapping(value = "/name/{name}")
  public Platform getByName(@PathVariable String name) {
    return platformRepository.getByName(name);
  }
}
