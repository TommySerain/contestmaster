package co.contestmaster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContestMasterApplication {

  public static void main(String[] args) {
    SpringApplication.run(ContestMasterApplication.class, args);
  }
}
