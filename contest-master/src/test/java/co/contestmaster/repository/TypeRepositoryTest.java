package co.contestmaster.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import co.contestmaster.entity.Type;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TypeRepositoryTest {

  @Autowired TypeRepository typeRepository;

  @Test
  void testGetByNameSucces() throws JsonProcessingException {
    Type type = typeRepository.getByName("SPORT");

    assertEquals(1, type.getId());
    assertEquals("SPORT", type.getName());
    assertEquals("Tournoi de sport en physique", type.getDescription());
  }

  @Test
  void testGetByNameFailed() {
    Type type = typeRepository.getByName("BLOB");
    assertNull(type);
  }
}
