SET foreign_key_checks = 0;

DROP TABLE IF EXISTS game;

DROP TABLE IF EXISTS `type`;

DROP TABLE IF EXISTS platform;

DROP TABLE IF EXISTS city;

DROP TABLE IF EXISTS party;

DROP TABLE IF EXISTS tournament;

DROP TABLE IF EXISTS `user`;

DROP TABLE IF EXISTS `user_tournament`;

DROP TABLE IF EXISTS `avatar`;

DROP TABLE IF EXISTS `user_avatar`;

SET foreign_key_checks = 1;

CREATE TABLE
    city (
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        zipcode VARCHAR(255)
    ) ENGINE = INNODB DEFAULT CHARSET = utf8;

CREATE TABLE
    platform (
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        description TEXT
    ) ENGINE = INNODB DEFAULT CHARSET = utf8;

CREATE TABLE
    `type` (
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        description TEXT
    ) ENGINE = INNODB DEFAULT CHARSET = utf8;

CREATE TABLE
    game (
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        description TEXT
    ) ENGINE = INNODB DEFAULT CHARSET = utf8;

CREATE TABLE
    `user_tournament` (
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
    ) ENGINE = INNODB DEFAULT CHARSET = utf8;

CREATE TABLE
    `user` (
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        pseudo VARCHAR(255) NOT NULL,
        description TEXT,
        email VARCHAR(320) NOT NULL,
        image VARCHAR(255),
        gold INT,
        isBot BOOLEAN NOT NULL,
        uid VARCHAR(255) NOT NULL UNIQUE
    ) ENGINE = INNODB DEFAULT CHARSET = utf8;

CREATE TABLE
    tournament (
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        team BOOLEAN,
        participantNbr INT NOT NULL,
        beginDate DATETIME NOT NULL,
        endDate DATETIME NOT NULL,
        image VARCHAR(255),
        address VARCHAR(255),
        reward VARCHAR(255),
        entry_price FLOAT,
        description TEXT,
        round_type VARCHAR(255),
        isPublic BOOLEAN
    ) ENGINE = INNODB DEFAULT CHARSET = utf8;

CREATE TABLE
    party (
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        date DATETIME,
        round VARCHAR(255) NOT NULL
    ) ENGINE = INNODB DEFAULT CHARSET = utf8;

CREATE TABLE
    avatar (
              id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
              name VARCHAR(255) NOT NULL,
              image VARCHAR(255) NOT NULL,
              cost INT NOT NULL,
              description VARCHAR(512)
) ENGINE = INNODB DEFAULT CHARSET = utf8;

CREATE TABLE
    `user_avatar` (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
) ENGINE = INNODB DEFAULT CHARSET = utf8;

ALTER TABLE `user`
ADD city INT,
ADD
    FOREIGN KEY (city) REFERENCES city (id) ON DELETE CASCADE;

ALTER TABLE `user`
ADD tournament INT,
ADD
    FOREIGN KEY (tournament) REFERENCES tournament (id);

ALTER TABLE `tournament`
ADD city INT NOT NULL,
ADD
    FOREIGN KEY (city) REFERENCES city (id) ON DELETE CASCADE;

ALTER TABLE `tournament`
ADD type INT NOT NULL,
ADD
    FOREIGN KEY (type) REFERENCES type (id) ON DELETE CASCADE;

ALTER TABLE `tournament`
ADD platform INT NOT NULL,
ADD
    FOREIGN KEY (platform) REFERENCES platform (id) ON DELETE CASCADE;

ALTER TABLE `tournament`
ADD game INT NOT NULL,
ADD
    FOREIGN KEY (game) REFERENCES game (id) ON DELETE CASCADE;

ALTER TABLE `tournament`
ADD planner INT,
ADD
    FOREIGN KEY (planner) REFERENCES user (id);

ALTER TABLE `party`
ADD winner INT,
ADD
    FOREIGN KEY (winner) REFERENCES user (id);

ALTER TABLE `party`
ADD user1 INT,
ADD
    FOREIGN KEY (user1) REFERENCES user (id);

ALTER TABLE `party`
ADD user2 INT,
ADD
    FOREIGN KEY (user2) REFERENCES user (id);

ALTER TABLE `party`
ADD tournament INT NOT NULL,
ADD
    FOREIGN KEY (tournament) REFERENCES tournament (id) ON DELETE CASCADE;

ALTER TABLE `user_tournament`
ADD user INT NOT NULL,
ADD
    FOREIGN KEY (user) REFERENCES user (id) ON DELETE CASCADE;

ALTER TABLE `user_tournament`
ADD tournament INT NOT NULL,
ADD
    FOREIGN KEY (tournament) REFERENCES tournament (id) ON DELETE CASCADE;

ALTER TABLE `user_avatar`
    ADD user INT NOT NULL,
ADD
    FOREIGN KEY (user) REFERENCES user (id) ON DELETE CASCADE;

ALTER TABLE `user_avatar`
    ADD avatar INT NOT NULL,
ADD
    FOREIGN KEY (avatar) REFERENCES avatar (id) ON DELETE CASCADE;

