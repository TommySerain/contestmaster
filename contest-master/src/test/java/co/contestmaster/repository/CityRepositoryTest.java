package co.contestmaster.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import co.contestmaster.entity.City;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
class CityRepositoryTest {

  @Autowired private CityRepository cityRepository;

  @Test
  @Order(1)
  void getAll() throws JSONException, IOException {
    List<City> cities = new ArrayList<City>(cityRepository.getAll());
    assertEquals(1, cities.get(0).getId());
    assertEquals("LYON", cities.get(0).getName());
    assertEquals("69000", cities.get(0).getZipcode());
  }

  @Test
  @Order(2)
  void getByIdSuccess() throws JSONException, IOException {

    City bddCity = cityRepository.getById(1);

    assertEquals(1, bddCity.getId());
    assertEquals("LYON", bddCity.getName());
    assertEquals("69000", bddCity.getZipcode());
  }

  @Test
  @Order(3)
  void getByIdFailed() throws JSONException, IOException {
    City city = cityRepository.getById(5);
    assertNull(city);
  }

  @Test
  @Order(4)
  void testGetByZipCodesuccess() throws JSONException, IOException {
    ArrayList<City> bddCityZipCode = new ArrayList<City>(cityRepository.getByZipCode("69000"));
    assertEquals(1, bddCityZipCode.size());
    assertEquals(1, bddCityZipCode.get(0).getId());
    assertEquals("LYON", bddCityZipCode.get(0).getName());
    assertEquals("69000", bddCityZipCode.get(0).getZipcode());
  }

  @Test
  @Order(5)
  void testGetByZipCodefailed() throws JSONException, IOException {
    ArrayList<City> bddCityZipCode = new ArrayList<City>(cityRepository.getByZipCode("73596"));
    ArrayList<City> expectedCities = new ArrayList<>();
    assertEquals(expectedCities, bddCityZipCode);
  }

  @Test
  @Order(6)
  void testGetByNameSuccess() throws JSONException, IOException {
    City city = new City(1, "LYON", "69000");
    assertEquals(1, city.getId());
    assertEquals("LYON", city.getName());
    assertEquals("69000", city.getZipcode());
  }

  @Test
  @Order(7)
  void testGetByNameFailed() throws JSONException, IOException {
    City city = cityRepository.getByName("FIDO");
    assertEquals(null, city);
  }

  @Test
  @Order(8)
  void Add() {
    City city = new City("Rillieux", "69286");
    Boolean cityIsAdded = cityRepository.add(city);
    assertTrue(cityIsAdded);
  }

  @Test
  @Order(9)
  void updateSuccess() {
    City city = new City("Lyon", "69286");
    Boolean cityIsUpdated = cityRepository.update(city, 1);
    assertTrue(cityIsUpdated);
  }

  @Test
  @Order(12)
  void deleteFailed() {
    Boolean cityIsDelete = cityRepository.delete(10);
    assertFalse(cityIsDelete);
  }
}
