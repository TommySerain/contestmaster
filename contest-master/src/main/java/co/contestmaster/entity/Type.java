package co.contestmaster.entity;

public class Type {
  private Integer id;
  private String name;
  private String description;

  public Type() {}

  public Type(Integer id) {
    this.id = id;
  }

  public Type(String name, String description) {
    this.name = name;
    this.description = description;
  }

  public Type(Integer id, String name, String description) {
    this.id = id;
    this.name = name;
    this.description = description;
  }

  /**
   * @return Integer return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return String return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return String return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }
}
