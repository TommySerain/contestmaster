package co.contestmaster.entity;

public class UserAvatar {
  private Integer id;
  private User user;
  private Avatar avatar;

  public UserAvatar() {
  }

  public UserAvatar(Integer id) {
    this.id = id;
  }

  public UserAvatar(User user, Avatar avatar) {
    this.user = user;
    this.avatar = avatar;
  }

  public UserAvatar(Integer id, User user, Avatar avatar) {
    this.id = id;
    this.user = user;
    this.avatar = avatar;
  }

  public UserAvatar(Integer id, int user_id, int avatar) {
    this.id = id;
    this.user = new User(user_id);
    this.avatar = new Avatar(avatar);
  }

  // Getters and setters
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Avatar getAvatar() {
    return avatar;
  }

  public void setAvatar(Avatar avatar) {
    this.avatar = avatar;
  }

  public int getUserId() {
    return user.getId();
  }

  public int getAvatarId() {
    return avatar.getId();
  }

  public void setUserId(int userId) {
    this.user = new User(userId);
  }

  public void setAvatarId(int avatarId) {
    this.avatar = new Avatar(avatarId);
  }
}