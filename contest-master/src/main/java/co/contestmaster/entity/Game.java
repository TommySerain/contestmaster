package co.contestmaster.entity;

public class Game {
  private Integer id;
  private String name;
  private String description;

  public Game() {}

  public Game(Integer id) {
    this.id = id;
  }

  public Game(String name, String description) {
    this.name = name;
    this.description = description;
  }

  public Game(Integer id, String name, String description) {
    this.id = id;
    this.name = name;
    this.description = description;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
