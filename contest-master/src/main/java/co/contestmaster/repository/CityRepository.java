package co.contestmaster.repository;

import co.contestmaster.entity.City;
import co.contestmaster.repository.interfaces.CityRepositoryInterface;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CityRepository extends AbstractRepository<City> implements CityRepositoryInterface {
  @Autowired private DataSource dataSource;
  private String getByZipCodeSql = "SELECT * FROM city WHERE zipcode=?";
  private String getByNameSql = "SELECT * FROM city WHERE name=?";

  public CityRepository() {
    this.addSQL = "INSERT INTO city (name,zipcode) VALUES (?,?)";
    this.getAllSQL = "SELECT * FROM city";
    this.getByIdSQL = "SELECT * FROM city WHERE id=?";
    this.updateSQL = "UPDATE city SET name=?,zipcode=? WHERE id=?";
    this.deleteSQL = "DELETE FROM city WHERE id=?";
  }

  @Override
  protected void setEntityId(City city, int id) {
    city.setId(id);
  }

  @Override
  protected void entityBindValues(PreparedStatement stmt, City city) throws SQLException {
    stmt.setString(1, city.getName());
    stmt.setString(2, city.getZipcode());
  }

  @Override
  protected void entityBindValuesWithId(PreparedStatement stmt, City city, Integer id)
      throws SQLException {
    entityBindValues(stmt, city);
    stmt.setInt(3, id);
  }

  @Override
  protected City sqlToEntity(ResultSet rs) throws SQLException {
    Integer id = rs.getInt("id");
    String name = rs.getString("name");
    String zipcode = rs.getString("zipcode");
    return new City(id, name, zipcode);
  }

  @Override
  public List<City> getByZipCode(String zipcode) {
    List<City> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByZipCodeSql);
      stmt.setString(1, zipcode);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        list.add(sqlToEntity(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public City getByName(String name) {
    City city;
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByNameSql);
      stmt.setString(1, name);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        city = sqlToEntity(rs);
        return city;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
}
