package co.contestmaster.entity;

public class Avatar {
  private Integer id;
  private String name;
  private String image;
  private Integer cost;
  private String description;

  public Avatar() {}

  public Avatar(Integer id) {
    this.id = id;
  }

  public Avatar(String name, String image, Integer cost, String description) {
    this.name = name;
    this.image = image;
    this.cost = cost;
    this.description = description;
  }

  public Avatar(Integer id, String name, String image, Integer cost, String description) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.cost = cost;
    this.description = description;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public Integer getCost() {
    return cost;
  }

  public void setCost(Integer cost) {
    this.cost = cost;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
