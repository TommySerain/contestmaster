package co.contestmaster.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;

import co.contestmaster.entity.Tournament;
import co.contestmaster.entity.User;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UserRepositoryTest {

  @Autowired UserRepository userRepository;

  @Test
  void testGetByCitySuccess() {
    ArrayList<User> users = new ArrayList<User>(userRepository.getByCity("LYON"));

    assertEquals(1, users.size());
    assertEquals(2, users.get(0).getId());
    assertEquals("Paul", users.get(0).getName());
    assertEquals("Joker", users.get(0).getPseudo());
    assertEquals("Solo", users.get(0).getDescription());
    assertEquals("paul.klein@gmail.com", users.get(0).getEmail());
  }

  @Test
  void testGetByCityFailed() {
    ArrayList<User> users = new ArrayList<User>(userRepository.getByCity("BLOB"));
    ArrayList<Tournament> expectedList = new ArrayList<Tournament>();
    assertEquals(expectedList, users);
  }

  @Test
  void testGetByPseudoSuccess() {
    User user = userRepository.getByPseudo("Joker");

    assertEquals(2, user.getId());
    assertEquals("Paul", user.getName());
    assertEquals("Joker", user.getPseudo());
    assertEquals("Solo", user.getDescription());
    assertEquals("paul.klein@gmail.com", user.getEmail());
    assertFalse(user.isBot());
  }

  @Test
  void testGetByPseudoFailed() {
    User user = (userRepository.getByPseudo("BLOB"));
    assertNull(user);
  }
}
