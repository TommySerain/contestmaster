## **<span style="color:orange">CONTEST MASTER BACKEND</span>**
(On retrouve le port exposé pour le serveur back dans application.properties: server.port=8048)  

### Pour activer le hook pre-push de formatage ###
git config core.hooksPath .githooks  

### *<span style="color:orange">DOCKER</span>*
    LANCER DOCKER
Vérifier la présence du fichier docker-compose.yml.
````shell
doker compose up -d 
````
'-d' permet de lancer en background  
**Possibilité d'ajouter à la suite le nom du service pour démarrer le service en question.**  
Exemple: docker compose up -d db lance seulement le service bdd
COMMANDES DOCKER
````shell
docker ps
````
*permet de consulter les container*s en cours

<u>Liste des services</u>:
- [x] Base de données MySQL
- [x] SonarQube Scanner
- [ ] Jenkins / GITLAB CI
- [ ] Serveur pour les tests


<span style="color:orange">SERVICE BD (Base de données)</span>  
Si vous souhaitez paramétrer votre bdd sur un autre port que 3306, vous devez modifier 'application.properties'  
et 'docker-compose.yml'

````java
// application.properties
spring.datasource.url=jdbc:mysql://root:root@localhost:3310/contest_master_database?createDatabaseIfNotExist=true
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
server.port=8048
spring.sql.init.mode=always
spring.application.name=contest-master
````

````dockerfile
(docker-compose.yml)
version: '3.8'

services:
  db:
    image: mysql:latest
    container_name: contest-master-database
    environment:
      MYSQL_ROOT_PASSWORD: root
    volumes:
      - mysql-data:/var/lib/mysql
    ports:
      - "3306:3306"
    networks:
      - my-network

volumes:
  mysql-data:

networks:
  my-network:

````

## Deploiement

Le déploiement de staging se déclenche quand une branche est merge sur la branche develop.  
Un artifcat est créé à la fin du job "deploy-staging".  
  
Après avoir téléchargé l'artifact.zip, il faut le décompréssé puis ouvrir un terminal dans le dossier contestmater de cet artifact.  
-Dans le terminal, taper la commande : tar -xzf deploy-staging.tar.gz  
Cette commande décompresse le fichier .tar.gz.  
-Une fois fait, vérifier que le fichier docker-compose soit présent avec un ls.  
-Taper la commande : docker-compose --env-file .env.staging up -d  
Cette commande va lancer le fichier docker-compose en utilisant les variables du fichier .env.staging qui se trouvait dans le fichier .tar.gz.  
-Vérifier que les containers sont bien lancés.  