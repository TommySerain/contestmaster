package co.contestmaster.controller;

import co.contestmaster.business.UserBusiness;
import co.contestmaster.entity.User;
import co.contestmaster.repository.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/api/user")
public class UserController implements AbstractController<User> {
  @Autowired private UserRepository userRepository;

  @Autowired private UserBusiness userBusiness;

  @PostMapping(value = "/add")
  @ResponseStatus(HttpStatus.CREATED)
  public User add(@RequestBody User entity) {
    boolean added = userRepository.add(entity);
    if (!added) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    return entity;
  }

  @PatchMapping(value = "/{id}")
  public User update(@RequestBody User entity, @PathVariable Integer id) {
    User userToUpdate = getById(id);
    updateVerify(entity, userToUpdate);
    userRepository.update(entity, id);
    return userToUpdate;
  }

  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable Integer id) {
    if (!userRepository.delete(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(value = {"/", ""})
  public List<User> getAll() {
    List<User> usersFromDatabase = userRepository.getAll();
    usersFromDatabase = userBusiness.getAllinformations(usersFromDatabase);

    return usersFromDatabase;
  }

  @GetMapping(value = "/{id}")
  public User getById(@PathVariable Integer id) {
    User userFromDatabase = userRepository.getById(id);
    userFromDatabase = userBusiness.getInformations(userFromDatabase);
    return userFromDatabase;
  }

  @GetMapping(value = "/uid/{uid}")
  public User getByUId(@PathVariable String uid) {
    User userFromDatabase = userRepository.getByUid(uid);
    userFromDatabase = userBusiness.getInformations(userFromDatabase);
    return userFromDatabase;
  }

  public void updateVerify(User entity, User userToUpdate) {
    if (entity.getName() != null) {
      userToUpdate.setName(entity.getName());
    }
    if (entity.getPseudo() != null) {
      userToUpdate.setPseudo(entity.getPseudo());
    }
    if (entity.getDescription() != null) {
      userToUpdate.setDescription(entity.getDescription());
    }
    if (entity.getCity() != null) {
      userToUpdate.setCity(entity.getCity());
    }
    if (entity.getEmail() != null) {
      userToUpdate.setEmail(entity.getEmail());
    }
    if (entity.getImage() != null) {
      userToUpdate.setImage(entity.getImage());
    }
    if (entity.getTournament() != null) {
      userToUpdate.setTournament(entity.getTournament());
    }
    if (entity.getGold() != null) {
      userToUpdate.setGold(entity.getGold());
    }
    if (entity.isBot() != null) {
      userToUpdate.setIsBot(entity.isBot());
    }
  }

  @GetMapping(value = "/city/{city}")
  public List<User> getUsersByCity(@PathVariable String city) {
    return userRepository.getByCity(city);
  }

  @GetMapping(value = "/pseudo/{pseudo}")
  public User getUsersByPseudo(@PathVariable String pseudo) {
    return userRepository.getByPseudo(pseudo);
  }

  @GetMapping(value = "/tournament/{tournament}")
  public List<User> getByTournament(@PathVariable String tournament) {
    return userRepository.getUsersByTournament(tournament);
  }
}
