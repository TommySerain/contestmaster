package co.contestmaster.repository.interfaces;

import co.contestmaster.entity.UserTournament;

public interface UserTournamentRepositoryInterface extends CrudRepository<UserTournament> {}
