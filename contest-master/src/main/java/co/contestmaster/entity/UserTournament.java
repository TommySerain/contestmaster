package co.contestmaster.entity;

public class UserTournament {
  private Integer id;
  private User user;
  private Tournament tournament;

  public UserTournament() {}

  public UserTournament(Integer id) {
    this.id = id;
  }

  public UserTournament(User user, Tournament tournament) {
    this.user = user;
    this.tournament = tournament;
  }

  public UserTournament(Integer id, User user, Tournament tournament) {
    this.id = id;
    this.user = user;
    this.tournament = tournament;
  }

  public UserTournament(Integer id, int user_id, int tournament) {
    this.id = id;
    this.user = new User(user_id);
    this.tournament = new Tournament(tournament);
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Tournament getTournament() {
    return tournament;
  }

  public void setTournament(Tournament tournament) {
    this.tournament = tournament;
  }
}
