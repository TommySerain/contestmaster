package co.contestmaster.controller;

import co.contestmaster.entity.Game;
import co.contestmaster.repository.GameRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/api/game")
public class GameController implements AbstractController<Game> {
  @Autowired private GameRepository gameRepository;

  @PostMapping(value = "/add")
  @ResponseStatus(HttpStatus.CREATED)
  public Game add(@RequestBody Game entity) {
    boolean added = gameRepository.add(entity);
    if (!added) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    return entity;
  }

  @PatchMapping(value = "/{id}")
  public Game update(@RequestBody Game entity, @PathVariable Integer id) {
    Game gameToUpdate = getById(id);
    if (entity.getName() != null) {
      gameToUpdate.setName(entity.getName());
    }
    if (entity.getDescription() != null) {
      gameToUpdate.setDescription(entity.getDescription());
    }
    gameRepository.update(gameToUpdate, id);
    return gameToUpdate;
  }

  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable Integer id) {
    if (!gameRepository.delete(id)) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping(value = {"/", ""})
  public List<Game> getAll() {
    return gameRepository.getAll();
  }

  @GetMapping(value = "/{id}")
  public Game getById(@PathVariable Integer id) {
    return gameRepository.getById(id);
  }

  @GetMapping(value = "/name/{name}")
  public Game getByName(@PathVariable String name) {
    return gameRepository.getByName(name);
  }
}
