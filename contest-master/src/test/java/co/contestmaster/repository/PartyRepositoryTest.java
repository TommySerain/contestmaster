package co.contestmaster.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import co.contestmaster.entity.Party;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PartyRepositoryTest {

  @Autowired UserRepository userRepository;
  @Autowired TournamentRepository tournamentRepository;
  @Autowired PartyRepository partyRepository;

  @Test
  void testGetByRoundSuccess() throws JsonProcessingException {
    LocalDateTime dateTime = LocalDateTime.of(2024, 10, 26, 15, 00, 00);
    List<Party> parties = partyRepository.getByRoundByTournament("demi_finale", 1);
    assertEquals(1, parties.size());
    assertEquals(1, parties.get(0).getId());
    assertEquals(dateTime, parties.get(0).getDate());
    assertEquals(null, parties.get(0).getWinner().getName());
    assertEquals(1, parties.get(0).getUser1().getId());
    assertEquals(4, parties.get(0).getUser2().getId());
    assertEquals("Demi_finale", parties.get(0).getRound());
    assertEquals(1, parties.get(0).getTournament().getId());
  }

  @Test
  void testGetByRoundFailed() {

    Party party = partyRepository.getById(5);

    assertNull(party);
  }
}
