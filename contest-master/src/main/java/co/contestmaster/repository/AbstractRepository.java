package co.contestmaster.repository;

import co.contestmaster.repository.interfaces.CrudRepository;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public abstract class AbstractRepository<E> implements CrudRepository<E> {
  @Autowired private DataSource dataSource;

  protected String addSQL;
  protected String getAllSQL;
  protected String getByIdSQL;
  protected String updateSQL;
  protected String deleteSQL;

  public List<E> getAll() {
    List<E> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(getAllSQL);
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        E entity = sqlToEntity(rs);
        list.add(entity);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public E getById(Integer id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(getByIdSQL);
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        E entity = sqlToEntity(rs);
        return entity;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public boolean delete(Integer id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(deleteSQL);
      stmt.setInt(1, id);

      return stmt.executeUpdate() == 1;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean add(E entity) {

    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(addSQL, Statement.RETURN_GENERATED_KEYS);
      entityBindValues(stmt, entity);
      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      rs.next();
      setEntityId(entity, rs.getInt(1));

      return true;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean update(E entity, Integer id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(updateSQL);
      entityBindValuesWithId(stmt, entity, id);

      return stmt.executeUpdate() == 1;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  protected abstract void setEntityId(E entity, int id);

  protected abstract void entityBindValues(PreparedStatement stmt, E entity) throws SQLException;

  protected abstract void entityBindValuesWithId(PreparedStatement stmt, E entity, Integer id)
      throws SQLException;

  protected abstract E sqlToEntity(ResultSet rs) throws SQLException;
}
