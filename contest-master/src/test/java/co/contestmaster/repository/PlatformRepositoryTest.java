package co.contestmaster.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import co.contestmaster.entity.Platform;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PlatformRepositoryTest {

  @Autowired PlatformRepository platformRepository;

  @Test
  void testGetByNameSuccess() throws JsonProcessingException {
    Platform platform = platformRepository.getByName("PS5");

    assertEquals(1, platform.getId());
    assertEquals("PS5", platform.getName());
    assertEquals("Console Playstation 5", platform.getDescription());
  }

  @Test
  void testGetByNameFailed() {
    Platform platform = platformRepository.getByName("Blob");
    assertNull(platform);
  }
}
