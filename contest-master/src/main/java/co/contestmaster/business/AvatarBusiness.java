package co.contestmaster.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import co.contestmaster.entity.UserAvatar;
import co.contestmaster.exception.AvatarAlreadyAssignedException;
import co.contestmaster.repository.UserAvatarRepository;

@Service
public class AvatarBusiness {

    @Autowired
    private UserAvatarRepository userAvatarRepository;

    public boolean addAvatarToUser(int userId, int avatarId) {
        var avatars = userAvatarRepository.getAll();
        for (var avatar : avatars) {
            if (avatar.getUserId() == userId && avatar.getAvatarId() == avatarId) {
                throw new AvatarAlreadyAssignedException("Avatar already added to user");
            }
        }
        UserAvatar entity = new UserAvatar();
        entity.setAvatarId(avatarId);
        entity.setUserId(userId);
        return userAvatarRepository.add(entity);
    }
}