package co.contestmaster.repository;

import co.contestmaster.entity.Game;
import co.contestmaster.repository.interfaces.GameRepositoryInterface;
import java.sql.*;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class GameRepository extends AbstractRepository<Game> implements GameRepositoryInterface {
  @Autowired private DataSource dataSource;
  private String getByNameSql = "SELECT * FROM game WHERE name=?";

  public GameRepository() {
    this.addSQL = "INSERT INTO game (name,description) VALUES (?,?)";
    this.getAllSQL = "SELECT * FROM game";
    this.getByIdSQL = "SELECT * FROM game WHERE id=?";
    this.updateSQL = "UPDATE game SET name=?,description=? WHERE id=?";
    this.deleteSQL = "DELETE FROM game WHERE id=?";
  }

  @Override
  protected void setEntityId(Game game, int id) {
    game.setId(id);
  }

  @Override
  protected void entityBindValues(PreparedStatement stmt, Game game) throws SQLException {
    stmt.setString(1, game.getName());
    stmt.setString(2, game.getDescription());
  }

  @Override
  protected void entityBindValuesWithId(PreparedStatement stmt, Game game, Integer id)
      throws SQLException {
    entityBindValues(stmt, game);
    stmt.setInt(3, id);
  }

  @Override
  protected Game sqlToEntity(ResultSet rs) throws SQLException {
    Integer id = rs.getInt("id");
    String name = rs.getString("name");
    String description = rs.getString("description");
    return new Game(id, name, description);
  }

  public Game getByName(String name) {
    Game game;
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByNameSql);
      stmt.setString(1, name);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        game = sqlToEntity(rs);
        return game;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
}
