package co.contestmaster.controller;

import co.contestmaster.entity.Type;
import co.contestmaster.repository.TypeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/api/type")
public class TypeController implements AbstractController<Type> {
  @Autowired private TypeRepository typeRepository;

  @PostMapping(value = "/add")
  @ResponseStatus(HttpStatus.CREATED)
  public Type add(@RequestBody Type entity) {
    boolean added = typeRepository.add(entity);
    if (!added) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    return entity;
  }

  @PatchMapping(value = "/{id}")
  public Type update(@RequestBody Type entity, @PathVariable Integer id) {
    Type typeToUpdate = getById(id);
    if (entity.getName() != null) {
      typeToUpdate.setName(entity.getName());
    }
    if (entity.getDescription() != null) {
      typeToUpdate.setDescription(entity.getDescription());
    }
    typeRepository.update(typeToUpdate, id);
    return typeToUpdate;
  }

  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable Integer id) {
    if (!typeRepository.delete(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(value = {"/", ""})
  public List<Type> getAll() {
    return typeRepository.getAll();
  }

  @GetMapping(value = "/{id}")
  public Type getById(@PathVariable Integer id) {
    return typeRepository.getById(id);
  }

  @GetMapping(value = "/name/{name}")
  public Type getByName(@PathVariable String name) {
    return typeRepository.getByName(name);
  }
}
