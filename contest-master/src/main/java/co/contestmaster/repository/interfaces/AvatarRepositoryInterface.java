package co.contestmaster.repository.interfaces;

import co.contestmaster.entity.Avatar;
import java.util.List;

public interface AvatarRepositoryInterface extends CrudRepository<Avatar> {

  Avatar getByName(String name);

  Avatar getByCost(Integer cost);

  List<Avatar> getAvatarsByUserId(String userId);
}
