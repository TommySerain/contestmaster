DROP PROCEDURE IF EXISTS AdjustParticipants;

--Procédure pour vérifier si notre nombre de participants est puissance 2 et le cas échéant, ajouter des users bot pour avoir le bon nombre.
CREATE PROCEDURE AdjustParticipants(IN tournamentId INT, IN NbParticipants INT)
BEGIN

    DECLARE neededBots INT;
    SET neededBots = POWER(2, CEIL(LOG(NbParticipants + 1) / LOG(2))) - NbParticipants;

    IF (NbParticipants & (NbParticipants - 1)) != 0 THEN
        WHILE neededBots > 0 DO
            -- Insérer des joueurs bots dans la table Participants pour atteindre une puissance de deux
            INSERT INTO `user`(`name`, `pseudo`, `description`, `email`, `password`, `role`, `image`, `score`, `isBot`, `city`, `tournament`)
            VALUES (CONCAT('bot', tournamentId, '_', neededBots), CONCAT('bot', tournamentId, neededBots), CONCAT('bot', tournamentId, neededBots), CONCAT('bot', tournamentId, neededBots, '@bot.bot'), 'bot1234', 'ROLE_BOT', 'bot.png', 0, true, 1, tournamentId);
            SET neededBots = neededBots - 1;
        END WHILE;
    END IF;

END;

DROP TRIGGER IF EXISTS addToUserTournamentAfterInsert;

--trigger qui va se déclencher après la création d'un user en ajoutant directement un tournoi
--Il ajoutera un ligne à la table de liaison tournament_user.
CREATE TRIGGER addToUserTournamentAfterInsert
    AFTER INSERT ON `user` FOR EACH ROW
    BEGIN
        IF (NEW.tournament IS NOT NULL) THEN
            INSERT INTO `user_tournament`(`user`, `tournament`) VALUES (NEW.id, NEW.tournament);
        END IF;
    END;

    DROP TRIGGER IF EXISTS addToUserTournamentAfterUpdate;

--trigger qui va se déclencher après la modification de la colonne tournament d'un user à condition qu'il ne soit pas null
--Il ajoutera un ligne à la table de liaison tournament_user.
CREATE TRIGGER addToUserTournamentAfterUpdate
    AFTER UPDATE ON `user` FOR EACH ROW
    BEGIN
        IF (NEW.tournament IS NOT NULL AND (NEW.tournament <> OLD.tournament OR OLD.tournament IS NULL)) THEN
            INSERT INTO `user_tournament`(`user`, `tournament`) VALUES (NEW.id, NEW.tournament);
        END IF;
    END;



DROP TRIGGER IF EXISTS updateWinnerOnBotUserUpdate;

--Trigger qui se déclenchera au moment ou l'on modifiera une partie pour ajouter un user2 si ce user est un BOT
--Il définiera le user1 comme gagnant de la partie.
CREATE TRIGGER updateWinnerOnBotUserUpdate
AFTER UPDATE ON Party FOR EACH ROW
BEGIN
    IF (OLD.user2 <> NEW.user2 AND NEW.user2 IN (SELECT id FROM user WHERE role = 'ROLE_BOT')) THEN
        UPDATE `party` SET `winner` = OLD.user1 WHERE id = OLD.id;
    END IF;
END;
--ATTENTION FONCTIONNE MAIS N'AJOUTE PAS DANS LE TABLEAU PARTIESWINNERS QUI N'EST PAS UNE PROPRIETE DE LA TABLE,
--IL FAUDRA LE FAIRE DANS LE BUSINESS EN PASSANT USER1 DANS LE TABLEAU WINNER QUAND ON AJOUTE LE BOT !

DROP PROCEDURE IF EXISTS DeleteTournamentBots;

--Cette procédure supprimera les bots d'un tournoi quand le round sera validé.
CREATE PROCEDURE DeleteTournamentBots(IN tournamentId INT)
BEGIN

    DECLARE userId INT;
    
    DECLARE botUsers CURSOR FOR
        SELECT id FROM user WHERE role = 'ROLE_BOT' AND tournament = tournamentId;
    
    OPEN botUsers;
    
    botUsersLoop: LOOP
        FETCH botUsers INTO userId;
        
        IF userId IS NULL THEN
            LEAVE botUsersLoop;
        END IF;
        
        DELETE FROM user WHERE id = userId;
    END LOOP botUsersLoop;
    
    CLOSE botUsers;
END;

SHOW TRIGGERS;