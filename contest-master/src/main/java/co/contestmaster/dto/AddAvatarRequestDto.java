package co.contestmaster.dto;

public class AddAvatarRequestDto {
    private int avatarId;

    public int getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(int avatarId) {
        this.avatarId = avatarId;
    }
}