package co.contestmaster.repository.interfaces;

import co.contestmaster.entity.Game;

public interface GameRepositoryInterface extends CrudRepository<Game> {

  Game getByName(String name);
}
