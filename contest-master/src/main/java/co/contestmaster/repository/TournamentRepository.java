package co.contestmaster.repository;

import co.contestmaster.entity.*;
import co.contestmaster.repository.interfaces.TournamentRepositoryInterface;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TournamentRepository extends AbstractRepository<Tournament>
    implements TournamentRepositoryInterface {
  @Autowired private DataSource dataSource;

  private String getByType =
      "SELECT * FROM tournament INNER JOIN type ON tournament.type = type.id WHERE type.name=?";
  private String getByCity =
      "SELECT * FROM tournament INNER JOIN city ON tournament.city = city.id WHERE city.name=?";
  private String getByPlatform =
      "SELECT * FROM tournament INNER JOIN platform ON tournament.platform = platform.id WHERE platform.name=?";
  private String getByGame =
      "SELECT * FROM tournament INNER JOIN game ON tournament.game = game.id WHERE game.name=?";
  private String getByBeginDate = "SELECT * FROM tournament WHERE beginDate=?";
  private String getByEndDate = "SELECT * FROM tournament WHERE endDate=?";
  private String getByName = "SELECT * FROM tournament WHERE name=?";
  private String getTournamentsByUser =
      "SELECT * FROM tournament INNER JOIN user_tournament ON tournament.id = user_tournament.tournament INNER JOIN user ON user.id = user_tournament.user WHERE user.name LIKE ?";
  private String getNbPartyNotFinishedByRoundSql =
      "SELECT COUNT(*) FROM party INNER JOIN tournament ON party.tournament = tournament.id WHERE party.winner is NULL AND party.round=? AND tournament.id=? GROUP BY party.round;";
  private String adjustParticipantsProcedureSql = "{CALL AdjustParticipants(?,?)}";
  private String deleteTournamentBotsSql = "{CALL DeleteTournamentBots(?)}";

  public TournamentRepository() {
    this.addSQL =
        "INSERT INTO tournament (name,participantNbr,beginDate,endDate,image,address,city,type,reward,entry_price,description,platform,game, planner,round_type,isPublic) "
            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    this.getAllSQL = "SELECT * FROM tournament";
    this.getByIdSQL = "SELECT * FROM tournament WHERE id=?";
    this.updateSQL =
        "UPDATE tournament SET name=?,participantNbr=?,beginDate=?,endDate=?,image=?,address=?,city=?,type=?,reward=?,entry_price=?,description=?,platform=?,game=?,planner=?,round_type=?,isPublic=?"
            + " WHERE id=?";
    this.deleteSQL = "DELETE FROM tournament WHERE id=?";
  }

  @Override
  protected void setEntityId(Tournament tournament, int id) {
    tournament.setId(id);
  }

  @Override
  protected void entityBindValues(PreparedStatement stmt, Tournament tournament)
      throws SQLException {
    stmt.setString(1, tournament.getName());
    stmt.setInt(2, tournament.getParticipantNbr());
    stmt.setTimestamp(3, java.sql.Timestamp.valueOf(tournament.getBeginDate()));
    stmt.setTimestamp(4, java.sql.Timestamp.valueOf(tournament.getEndDate()));
    stmt.setString(5, tournament.getImage());
    stmt.setString(6, tournament.getAddress());
    stmt.setInt(7, tournament.getCity().getId());
    stmt.setInt(8, tournament.getType_id().getId());
    stmt.setString(9, tournament.getReward());
    stmt.setFloat(10, tournament.getEntryPrice());
    stmt.setString(11, tournament.getDescription());
    stmt.setInt(12, tournament.getPlatform().getId());
    stmt.setInt(13, tournament.getGame().getId());
    stmt.setInt(14, tournament.getPlanner().getId());
    stmt.setString(15, tournament.getRound_type());
    stmt.setBoolean(16, tournament.isPublic());
  }

  @Override
  protected void entityBindValuesWithId(PreparedStatement stmt, Tournament tournament, Integer id)
      throws SQLException {
    entityBindValues(stmt, tournament);
    stmt.setInt(17, id);
  }

  @Override
  public Tournament sqlToEntity(ResultSet rs) throws SQLException {
    Integer id = rs.getInt("id");
    String name = rs.getString("name");
    boolean team = rs.getBoolean("team");
    Integer participantNbr = rs.getInt("participantNbr");
    java.sql.Timestamp timestamp = rs.getTimestamp("beginDate");
    LocalDateTime beginDate = timestamp.toLocalDateTime();
    java.sql.Timestamp timestamp2 = rs.getTimestamp("endDate");
    LocalDateTime endDate = timestamp2.toLocalDateTime();
    String image = rs.getString("image");
    String address = rs.getString("address");
    Integer city = rs.getInt("city");
    Integer type = rs.getInt("type");
    String reward = rs.getString("reward");
    Float entry_price = rs.getFloat("entry_price");
    String description = rs.getString("description");
    Integer platform = rs.getInt("platform");
    Integer game = rs.getInt("game");
    Integer planner = rs.getInt("planner");
    String round_type = rs.getString("round_type");
    Boolean isPublic = rs.getBoolean("isPublic");

    return new Tournament(
        id,
        name,
        team,
        participantNbr,
        beginDate,
        endDate,
        image,
        address,
        city,
        type,
        reward,
        entry_price,
        description,
        platform,
        game,
        planner,
        round_type,
        isPublic);
  }

  @Override
  public List<Tournament> getByType(String type) {
    List<Tournament> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByType);
      stmt.setString(1, type);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        list.add(sqlToEntity(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public List<Tournament> getByCity(String city) {
    List<Tournament> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByCity);
      stmt.setString(1, city);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        list.add(sqlToEntity(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public List<Tournament> getByPlatform(String platform) {
    List<Tournament> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByPlatform);
      stmt.setString(1, platform);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        list.add(sqlToEntity(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public List<Tournament> getByGame(String game) {
    List<Tournament> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByGame);
      stmt.setString(1, game);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        list.add(sqlToEntity(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public List<Tournament> getByBeginDate(LocalDateTime date) {
    List<Tournament> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByBeginDate);
      stmt.setTimestamp(1, java.sql.Timestamp.valueOf(date));
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        list.add(sqlToEntity(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public List<Tournament> getByEndDate(LocalDateTime date) {
    List<Tournament> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByEndDate);
      stmt.setTimestamp(1, java.sql.Timestamp.valueOf(date));
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        list.add(sqlToEntity(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public Tournament getByName(String name) {
    Tournament tournament;
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByName);
      stmt.setString(1, name);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        tournament = sqlToEntity(rs);
        return tournament;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public List<Tournament> getTournamentsByUser(String name) {
    List<Tournament> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      String query = "%" + name + "%";
      PreparedStatement stmt = connection.prepareStatement(this.getTournamentsByUser);
      stmt.setString(1, query);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        list.add(sqlToEntity(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public Integer getNbPartyNotFinishedByRound(String round, Tournament tournament) {
    Integer NbMatchNotFinished = 0;
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getNbPartyNotFinishedByRoundSql);
      stmt.setString(1, round);
      stmt.setInt(2, tournament.getId());
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        NbMatchNotFinished++;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return NbMatchNotFinished;
  }

  public void adjustParticipants(int tournamentId, int nbParticipants) {
    try (Connection connection = dataSource.getConnection();
        CallableStatement statement = connection.prepareCall(adjustParticipantsProcedureSql)) {
      statement.setInt(1, tournamentId);
      statement.setInt(2, nbParticipants);
      statement.execute();
      System.out.println("La procédure stockée AdjustParticipants a été exécutée avec succès.");
    } catch (SQLException e) {
      e.printStackTrace();
      System.out.println(
          "Erreur lors de l'exécution de la procédure stockée AdjustParticipants : "
              + e.getMessage());
    }
  }

  public void deleteTournamentBots(int tournamentId) {
    try (Connection connection = dataSource.getConnection();
        CallableStatement statement = connection.prepareCall(deleteTournamentBotsSql)) {
      statement.setInt(1, tournamentId);
      statement.execute();
      System.out.println("La procédure stockée deleteTournamentBots a été exécutée avec succès.");
    } catch (SQLException e) {
      e.printStackTrace();
      System.out.println(
          "Erreur lors de l'exécution de la procédure stockée deleteTournamentBots : "
              + e.getMessage());
    }
  }
}
