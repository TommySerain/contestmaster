package co.contestmaster.repository;

import co.contestmaster.entity.UserTournament;
import co.contestmaster.repository.interfaces.UserTournamentRepositoryInterface;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserTournamentRepository extends AbstractRepository<UserTournament>
    implements UserTournamentRepositoryInterface {

  public UserTournamentRepository() {
    this.addSQL = "INSERT INTO user_tournament (user,tournament) VALUES (?,?)";
    this.getAllSQL = "SELECT * FROM user_tournament";
    this.getByIdSQL = "SELECT * FROM user_tournament WHERE id=?";
    this.updateSQL = "UPDATE user_tournament SET user=?,tournament=? WHERE id=?";
    this.deleteSQL = "DELETE FROM user_tournament WHERE id=?";
  }

  @Override
  protected void setEntityId(UserTournament userTournament, int id) {
    userTournament.setId(id);
  }

  @Override
  protected void entityBindValues(PreparedStatement stmt, UserTournament userTournament)
      throws SQLException {
    stmt.setInt(1, userTournament.getUser().getId());
    stmt.setInt(2, userTournament.getTournament().getId());
  }

  @Override
  protected void entityBindValuesWithId(
      PreparedStatement stmt, UserTournament userTournament, Integer id) throws SQLException {
    entityBindValues(stmt, userTournament);
    stmt.setInt(3, id);
  }

  @Override
  protected UserTournament sqlToEntity(ResultSet rs) throws SQLException {
    Integer id = rs.getInt("id");
    Integer user = rs.getInt("user");
    Integer tournament = rs.getInt("tournament");
    return new UserTournament(id, user, tournament);
  }
}
