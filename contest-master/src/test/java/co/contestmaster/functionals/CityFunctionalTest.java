package co.contestmaster.functionals;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import co.contestmaster.repository.CityRepository;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CityFunctionalTest {

  @Autowired private MockMvc mvc;

  @Autowired private CityRepository cityRepository;

  @Test
  @Order(1)
  void testCreateAndRetrieveCity() throws Exception {
    // Créer une nouvelle ville
    mvc.perform(
            post("/api/city/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    """
                {
                    "name": "TestCity",
                    "zipcode": "12345"
                }
                """))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id").isNumber())
        .andExpect(jsonPath("$.name").value("TestCity"))
        .andExpect(jsonPath("$.zipcode").value("12345"));

    // Récupérer toutes les villes et vérifier que la nouvelle ville est présente
    mvc.perform(get("/api/city"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[?(@.name == 'TestCity')]").exists());

    // Récupérer la ville par son nom
    mvc.perform(get("/api/city/name/TestCity"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name").value("TestCity"))
        .andExpect(jsonPath("$.zipcode").value("12345"));

    // Mettre à jour la ville
    mvc.perform(
            patch("/api/city/{id}", cityRepository.getByName("TestCity").getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"zipcode\": \"54321\"}"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.zipcode").value("54321"));

    // Vérifier la mise à jour
    mvc.perform(get("/api/city/name/TestCity"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.zipcode").value("54321"));

    // Supprimer la ville
    mvc.perform(delete("/api/city/{id}", cityRepository.getByName("TestCity").getId()))
        .andExpect(status().isNoContent());

    // Vérifier que la ville a été supprimée
    mvc.perform(get("/api/city/name/TestCity")).andExpect(status().isNotFound());
  }
}
