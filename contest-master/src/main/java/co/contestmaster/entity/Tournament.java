package co.contestmaster.entity;

import java.time.LocalDateTime;
import java.util.List;

public class Tournament {
  private Integer id;
  private String name;
  private boolean team;
  private Integer participantNbr;
  private LocalDateTime beginDate;
  private LocalDateTime endDate;
  private String image;
  private String address;
  private City city;
  private Type type;
  private String reward;
  private Float entryPrice;
  private String description;
  private Platform platform;
  private Game game;
  private User planner;
  private String round_type;
  private User winner;
  private boolean isPublic;
  private List<User> partiesWinners;

  public Tournament() {}

  public Tournament(Integer id) {
    this.id = id;
  }

  public Tournament(
      String name,
      boolean team,
      Integer participantNbr,
      LocalDateTime beginDate,
      LocalDateTime endDate,
      String image,
      String address,
      City city,
      Type type,
      String reward,
      Float entryPrice,
      String description,
      Platform platform,
      Game game,
      User planner,
      String round_type,
      boolean isPublic) {
    this.name = name;
    this.team = team;
    this.participantNbr = participantNbr;
    this.beginDate = beginDate;
    this.endDate = endDate;
    this.image = image;
    this.address = address;
    this.city = city;
    this.type = type;
    this.reward = reward;
    this.entryPrice = entryPrice;
    this.description = description;
    this.platform = platform;
    this.game = game;
    this.planner = planner;
    this.round_type = round_type;
    this.isPublic = isPublic;
  }

  public Tournament(
      Integer id,
      String name,
      boolean team,
      Integer participantNbr,
      LocalDateTime beginDate,
      LocalDateTime endDate,
      String image,
      String address,
      City city,
      Type type,
      String reward,
      Float entryPrice,
      String description,
      Platform platform,
      Game game,
      User planner,
      String round_type,
      boolean isPublic) {
    this.id = id;
    this.name = name;
    this.team = team;
    this.participantNbr = participantNbr;
    this.beginDate = beginDate;
    this.endDate = endDate;
    this.image = image;
    this.address = address;
    this.city = city;
    this.type = type;
    this.reward = reward;
    this.entryPrice = entryPrice;
    this.description = description;
    this.platform = platform;
    this.game = game;
    this.planner = planner;
    this.round_type = round_type;
    this.isPublic = isPublic;
  }

  public Tournament(
      Integer id,
      String name,
      boolean team,
      Integer participantNbr,
      LocalDateTime beginDate,
      LocalDateTime endDate,
      String image,
      String address,
      Integer city,
      Integer type,
      String reward,
      Float entryPrice,
      String description,
      Integer platform,
      Integer game,
      Integer planner,
      String round_type,
      boolean isPublic) {
    this.id = id;
    this.name = name;
    this.team = team;
    this.participantNbr = participantNbr;
    this.beginDate = beginDate;
    this.endDate = endDate;
    this.image = image;
    this.address = address;
    this.city = new City(city);
    this.type = new Type(type);
    this.reward = reward;
    this.entryPrice = entryPrice;
    this.description = description;
    this.platform = new Platform(platform);
    this.game = new Game(game);
    this.planner = new User(planner);
    this.round_type = round_type;
    this.isPublic = isPublic;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isTeam() {
    return team;
  }

  public void setTeam(boolean team) {
    this.team = team;
  }

  public Integer getParticipantNbr() {
    return participantNbr;
  }

  public void setParticipantNbr(Integer participantNbr) {
    this.participantNbr = participantNbr;
  }

  public LocalDateTime getBeginDate() {
    return beginDate;
  }

  public void setBeginDate(LocalDateTime beginDate) {
    this.beginDate = beginDate;
  }

  public LocalDateTime getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDateTime endDate) {
    this.endDate = endDate;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public City getCity() {
    return city;
  }

  public void setCity(City city) {
    this.city = city;
  }

  public Type getType_id() {
    return type;
  }

  public void setType_id(Type type) {
    this.type = type;
  }

  public String getReward() {
    return reward;
  }

  public void setReward(String reward) {
    this.reward = reward;
  }

  public Float getEntryPrice() {
    return entryPrice;
  }

  public void setEntryPrice(Float entryPrice) {
    this.entryPrice = entryPrice;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Platform getPlatform() {
    return platform;
  }

  public void setPlatform(Platform platform) {
    this.platform = platform;
  }

  public Game getGame() {
    return game;
  }

  public void setGame(Game game) {
    this.game = game;
  }

  public User getPlanner() {
    return planner;
  }

  public void setPlanner(User planner) {
    this.planner = planner;
  }

  public String getRound_type() {
    return round_type;
  }

  public void setRound_type(String round_type) {
    this.round_type = round_type;
  }

  public User getWinner() {
    return winner;
  }

  public void setWinner(User winner) {
    this.winner = winner;
  }

  public boolean isPublic() {
    return isPublic;
  }

  public void setPublic(boolean isPublic) {
    this.isPublic = isPublic;
  }

  public List<User> getPartiesWinners() {
    return partiesWinners;
  }

  public void setPartiesWinners(List<User> partiesWinners) {
    this.partiesWinners = partiesWinners;
  }

  public void addPartiesWinners(User partiesWinners) {
    this.partiesWinners.add(partiesWinners);
  }
}
