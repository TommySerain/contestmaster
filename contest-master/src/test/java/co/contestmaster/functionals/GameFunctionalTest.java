package co.contestmaster.functionals;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import co.contestmaster.repository.GameRepository;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class GameFunctionalTest {

  @Autowired private MockMvc mvc;

  @Autowired private GameRepository gameRepository;

  @Test
  @Order(1)
  void testCreateAndRetrieveGame() throws Exception {
    // Créer un nouveau jeu
    mvc.perform(
            post("/api/game/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    """
                {
                    "name": "TestGame",
                    "description": "A test game description"
                }
                """))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id").isNumber())
        .andExpect(jsonPath("$.name").value("TestGame"))
        .andExpect(jsonPath("$.description").value("A test game description"));

    // Récupérer tous les jeux et vérifier que le nouveau jeu est présent
    mvc.perform(get("/api/game"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[?(@.name == 'TestGame')]").exists());

    // Récupérer le jeu par son nom
    mvc.perform(get("/api/game/name/TestGame"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name").value("TestGame"))
        .andExpect(jsonPath("$.description").value("A test game description"));

    // Mettre à jour le jeu
    mvc.perform(
            patch("/api/game/{id}", gameRepository.getByName("TestGame").getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"description\": \"Updated game description\"}"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.description").value("Updated game description"));

    // Vérifier la mise à jour
    mvc.perform(get("/api/game/name/TestGame"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.description").value("Updated game description"));

    // Supprimer le jeu
    mvc.perform(delete("/api/game/{id}", gameRepository.getByName("TestGame").getId()))
        .andExpect(status().isNoContent());
  }
}
