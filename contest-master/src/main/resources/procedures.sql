DROP PROCEDURE IF EXISTS AdjustParticipants$$

CREATE PROCEDURE AdjustParticipants(IN tournamentId INT, IN NbParticipants INT)
BEGIN

    DECLARE neededBots INT;
    SET neededBots = POWER(2, CEIL(LOG(NbParticipants + 1) / LOG(2))) - NbParticipants;

    IF (NbParticipants & (NbParticipants - 1)) != 0 THEN
        WHILE neededBots > 0 DO
            INSERT INTO `user`(`name`, `pseudo`, `description`, `email`, `image`, `gold`, `isBot`, `city`, `tournament`)
            VALUES (CONCAT('bot', tournamentId, '_', neededBots), CONCAT('bot', tournamentId, neededBots), CONCAT('bot', tournamentId, neededBots), CONCAT('bot', tournamentId, neededBots, '@bot.bot'), 'bot.png', 0, true, 1, tournamentId);
            SET neededBots = neededBots - 1;
        END WHILE;
    END IF;

END$$

DROP TRIGGER IF EXISTS addToUserTournamentAfterInsert$$

CREATE TRIGGER addToUserTournamentAfterInsert
    AFTER INSERT ON `user` FOR EACH ROW
    BEGIN
        IF (NEW.tournament IS NOT NULL) THEN
            INSERT INTO `user_tournament`(`user`, `tournament`) VALUES (NEW.id, NEW.tournament);
        END IF;
    END$$

    DROP TRIGGER IF EXISTS addToUserTournamentAfterUpdate$$

CREATE TRIGGER addToUserTournamentAfterUpdate
    AFTER UPDATE ON `user` FOR EACH ROW
    BEGIN
        IF (NEW.tournament IS NOT NULL AND (NEW.tournament <> OLD.tournament OR OLD.tournament IS NULL)) THEN
            INSERT INTO `user_tournament`(`user`, `tournament`) VALUES (NEW.id, NEW.tournament);
        END IF;
    END$$



DROP TRIGGER IF EXISTS updateWinnerOnBotUserUpdate$$

CREATE TRIGGER updateWinnerOnBotUserUpdate
AFTER UPDATE ON `party` FOR EACH ROW
BEGIN
    IF (OLD.user2 <> NEW.user2 AND NEW.user2 IN (SELECT id FROM user WHERE isBot=true)) THEN
        UPDATE `party` SET `winner` = OLD.user1 WHERE id = OLD.id;
    END IF;
END$$

DROP PROCEDURE IF EXISTS DeleteTournamentBots$$

CREATE PROCEDURE DeleteTournamentBots(IN tournamentId INT)
BEGIN

    DECLARE userId INT;
    
    DECLARE botUsers CURSOR FOR
        SELECT id FROM user WHERE isBot=true AND tournament = tournamentId;
    
    OPEN botUsers;
    
    botUsersLoop: LOOP
        FETCH botUsers INTO userId;
        
        IF userId IS NULL THEN
            LEAVE botUsersLoop;
        END IF;
        
        DELETE FROM user WHERE id = userId;
    END LOOP botUsersLoop;
    
    CLOSE botUsers;
END$$