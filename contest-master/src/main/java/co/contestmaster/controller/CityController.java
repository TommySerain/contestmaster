package co.contestmaster.controller;

import co.contestmaster.entity.City;
import co.contestmaster.repository.CityRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/api/city")
public class CityController implements AbstractController<City> {
  @Autowired private CityRepository cityRepository;

  @PostMapping(value = "/add")
  @ResponseStatus(HttpStatus.CREATED)
  public City add(@RequestBody City entity) {

    boolean added = cityRepository.add(entity);
    if (!added) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    return entity;
  }

  @PatchMapping(value = "/{id}")
  public City update(@RequestBody City entity, @PathVariable Integer id) {
    City cityToUpdate = getById(id);
    if (entity.getName() != null) {
      cityToUpdate.setName(entity.getName());
    }
    if (entity.getZipcode() != null) {
      cityToUpdate.setZipcode(entity.getZipcode());
    }
    cityRepository.update(cityToUpdate, id);
    return cityToUpdate;
  }

  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable Integer id) {
    if (!cityRepository.delete(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(value = {"/", ""})
  public List<City> getAll() {

    return cityRepository.getAll();
  }

  @GetMapping(value = "/{id}")
  public City getById(@PathVariable Integer id) {
    if (cityRepository.getById(id) == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    return cityRepository.getById(id);
  }

  @GetMapping(value = "/name/{name}")
  public City getByName(@PathVariable String name) {
    if (cityRepository.getByName(name) == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return cityRepository.getByName(name);
  }

  @GetMapping(value = "/zipcode/{zipcode}")
  public List<City> getByZipCode(@PathVariable String zipcode) {

    return cityRepository.getByZipCode(zipcode);
  }
}
