package co.contestmaster.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTest {

  @Autowired MockMvc mvc;

  @Test
  void testAdd() throws Exception {
    mvc.perform(
            post("/api/game/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    """
                                            {
                                                "name": "FORTNITE",
                                                "description": "Jeu sur PC, PS5"
                                            }
                                            """))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id").isNumber())
        .andExpect(jsonPath("$.name").value("FORTNITE"));
  }

  @Test
  void testGetAll() throws Exception {
    mvc.perform(get("/api/game"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").isArray())
        .andExpect(jsonPath("$[?(@.name == null)]").isEmpty())
        .andExpect(jsonPath("$[?(@.description == null)]").isEmpty());
  }

  @Test
  void testGetById() throws Exception {
    mvc.perform(get("/api/game/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name").value("FIFA"))
        .andExpect(jsonPath("$.id").value(1))
        .andExpect(jsonPath("$.description").value("Jeu de foot sur PC, PS5, XBOX"));
  }

  @Test
  void testUpdate() throws Exception {
    mvc.perform(
            patch("/api/game/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    """
                                            {
                                                "description": "Jeu de foot sur toutes plateformes"
                                            }
                                            """))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name").value("FIFA"))
        .andExpect(jsonPath("$.description").value("Jeu de foot sur toutes plateformes"))
        .andExpect(jsonPath("$.id").value(1));
  }

  //  @Test
  //  void testDelete() throws Exception {
  //    mvc.perform(delete("/api/game/6"))
  //            .andExpect(status().isNoContent());
  //  }

  @Test
  void testGetByName() throws Exception {
    mvc.perform(get("/api/game/name/FIFA"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name").value("FIFA"))
        .andExpect(jsonPath("$.id").value(1))
        .andExpect(jsonPath("$.description").value("Jeu de foot sur PC, PS5, XBOX"));
  }
}
