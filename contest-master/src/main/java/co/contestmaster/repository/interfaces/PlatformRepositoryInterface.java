package co.contestmaster.repository.interfaces;

import co.contestmaster.entity.Platform;

public interface PlatformRepositoryInterface extends CrudRepository<Platform> {

  Platform getByName(String name);
}
