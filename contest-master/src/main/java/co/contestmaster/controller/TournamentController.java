package co.contestmaster.controller;

import co.contestmaster.business.TournamentBusiness;
import co.contestmaster.entity.Tournament;
import co.contestmaster.repository.TournamentRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/api/tournament")
public class TournamentController implements AbstractController<Tournament> {
  @Autowired private TournamentRepository tournamentRepository;

  @Autowired private TournamentBusiness tournamentBusiness;

  @PostMapping(value = "/add")
  @ResponseStatus(HttpStatus.CREATED)
  public Tournament add(@RequestBody Tournament entity) {
    boolean added = tournamentRepository.add(entity);

    if (!added) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    return entity;
  }

  @PatchMapping(value = "/{id}")
  public Tournament update(@RequestBody Tournament entity, @PathVariable Integer id) {
    Tournament tournamentToUpdate = getById(id);
    updateVerify(entity, tournamentToUpdate);
    tournamentRepository.update(tournamentToUpdate, id);
    return tournamentToUpdate;
  }

  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable Integer id) {
    if (!tournamentRepository.delete(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(value = {"/", ""})
  public List<Tournament> getAll() {
    List<Tournament> tournamentsFromDatabase = tournamentRepository.getAll();

    tournamentsFromDatabase = tournamentBusiness.getAllinformations(tournamentsFromDatabase);

    return tournamentsFromDatabase;
  }

  @GetMapping(value = "/{id}")
  public Tournament getById(@PathVariable Integer id) {
    Tournament tournamentFromDatabase = tournamentRepository.getById(id);
    tournamentFromDatabase = tournamentBusiness.getInformations(tournamentFromDatabase);
    return tournamentFromDatabase;
  }

  @GetMapping(value = "/name/{name}")
  public Tournament getByName(@PathVariable String name) {
    Tournament tournamentFromDatabase = tournamentRepository.getByName(name);
    tournamentFromDatabase = tournamentBusiness.getInformations(tournamentFromDatabase);
    return tournamentFromDatabase;
  }

  @GetMapping(value = "/beginDate/{beginDate}")
  public List<Tournament> getByBeginDate(@PathVariable LocalDateTime beginDate) {
    List<Tournament> tournamentsFromDatabase = tournamentRepository.getByBeginDate(beginDate);
    List<Tournament> mappedTournaments = new ArrayList<>();
    mappedTournaments = tournamentBusiness.getAllinformations(tournamentsFromDatabase);

    return mappedTournaments;
  }

  @GetMapping(value = "/endDate/{endDate}")
  public List<Tournament> getByEndDate(@PathVariable LocalDateTime endDate) {
    List<Tournament> tournamentsFromDatabase = tournamentRepository.getByEndDate(endDate);
    List<Tournament> mappedTournaments = new ArrayList<>();
    mappedTournaments = tournamentBusiness.getAllinformations(tournamentsFromDatabase);

    return mappedTournaments;
  }

  @GetMapping(value = "/type/{type}")
  public List<Tournament> getByType(@PathVariable String type) {
    List<Tournament> tournamentsFromDatabase = tournamentRepository.getByType(type);
    List<Tournament> mappedTournaments = new ArrayList<>();
    mappedTournaments = tournamentBusiness.getAllinformations(tournamentsFromDatabase);

    return mappedTournaments;
  }

  @GetMapping(value = "/game/{game}")
  public List<Tournament> getByGame(@PathVariable String game) {
    List<Tournament> tournamentsFromDatabase = tournamentRepository.getByGame(game);
    List<Tournament> mappedTournaments = new ArrayList<>();
    mappedTournaments = tournamentBusiness.getAllinformations(tournamentsFromDatabase);

    return mappedTournaments;
  }

  @GetMapping(value = "/tournaments/{user}")
  public List<Tournament> getByUser(@PathVariable String userName) {
    List<Tournament> tournamentsFromDatabase = tournamentRepository.getTournamentsByUser(userName);
    List<Tournament> mappedTournaments = new ArrayList<>();
    mappedTournaments = tournamentBusiness.getAllinformations(tournamentsFromDatabase);

    return mappedTournaments;
  }

  private void updateVerify(Tournament entity, Tournament tournamentToUpdate) {
    if (entity.getName() != null) {
      tournamentToUpdate.setName(entity.getName());
    }
    if (entity.getParticipantNbr() != null) {
      tournamentToUpdate.setParticipantNbr(entity.getParticipantNbr());
    }
    if (entity.getBeginDate() != null) {
      tournamentToUpdate.setBeginDate(entity.getBeginDate());
    }
    if (entity.getEndDate() != null) {
      tournamentToUpdate.setEndDate(entity.getEndDate());
    }
    if (entity.getImage() != null) {
      tournamentToUpdate.setImage(entity.getImage());
    }
    if (entity.getAddress() != null) {
      tournamentToUpdate.setAddress(entity.getAddress());
    }
    if (entity.getCity() != null) {
      tournamentToUpdate.setCity(entity.getCity());
    }
    if (entity.getType_id() != null) {
      tournamentToUpdate.setType_id(entity.getType_id());
    }
    if (entity.getReward() != null) {
      tournamentToUpdate.setReward(entity.getReward());
    }
    if (entity.getEntryPrice() != null) {
      tournamentToUpdate.setEntryPrice(entity.getEntryPrice());
    }
    if (entity.getDescription() != null) {
      tournamentToUpdate.setDescription(entity.getDescription());
    }
    if (entity.getPlatform() != null) {
      tournamentToUpdate.setPlatform(entity.getPlatform());
    }
    if (entity.getGame() != null) {
      tournamentToUpdate.setGame(entity.getGame());
    }
    if (entity.getPlanner() != null) {
      tournamentToUpdate.setPlanner(entity.getPlanner());
    }
    if (entity.getRound_type() != null) {
      tournamentToUpdate.setRound_type(entity.getRound_type());
    }
    if (entity.getWinner() != null) {
      tournamentToUpdate.setWinner(entity.getWinner());
    }
    if (entity.isPublic() != tournamentToUpdate.isPublic()) {
      tournamentToUpdate.setPublic(entity.isPublic());
    }
  }
}
