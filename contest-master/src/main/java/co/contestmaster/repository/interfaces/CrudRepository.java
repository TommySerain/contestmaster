package co.contestmaster.repository.interfaces;

import java.util.List;

public interface CrudRepository<T> {
  List<T> getAll();

  T getById(Integer id);

  boolean add(T entity);

  boolean update(T entity, Integer id);

  boolean delete(Integer id);
}
