package co.contestmaster.repository.interfaces;

import co.contestmaster.entity.Type;

public interface TypeRepositoryInterface extends CrudRepository<Type> {
  Type getByName(String name);
}
