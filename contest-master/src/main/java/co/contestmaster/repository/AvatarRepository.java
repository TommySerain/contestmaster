package co.contestmaster.repository;

import co.contestmaster.entity.Avatar;
import co.contestmaster.repository.interfaces.AvatarRepositoryInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AvatarRepository extends AbstractRepository<Avatar>
    implements AvatarRepositoryInterface {
  @Autowired private DataSource dataSource;
  private String getByNameSql = "SELECT * FROM avatar WHERE name=?";
  private String getByCostSql = "SELECT * FROM avatar WHERE cost=?";
  private String getAvatarsByUserIdSQL =
      "SELECT * FROM avatar INNER JOIN user_avatar ON avatar.id = user_avatar.avatar INNER JOIN user ON user_avatar.user = user.id WHERE user.id = ?";

  public AvatarRepository() {
    this.addSQL = "INSERT INTO avatar (name, image, cost, description) VALUES (?,?)";
    this.getAllSQL = "SELECT * FROM avatar";
    this.getByIdSQL = "SELECT * FROM avatar WHERE id=?";
    this.updateSQL = "UPDATE avatar SET name=?, image=?, cost=?, description=? WHERE id=?";
    this.deleteSQL = "DELETE FROM avatar WHERE id=?";
  }

  @Override
  protected void setEntityId(Avatar avatar, int id) {
    avatar.setId(id);
  }

  @Override
  protected void entityBindValues(PreparedStatement stmt, Avatar avatar) throws SQLException {
    stmt.setString(1, avatar.getName());
    stmt.setString(1, avatar.getImage());
    stmt.setInt(1, avatar.getCost());
    stmt.setString(2, avatar.getDescription());
  }

  @Override
  protected void entityBindValuesWithId(PreparedStatement stmt, Avatar avatar, Integer id)
      throws SQLException {
    entityBindValues(stmt, avatar);
    stmt.setInt(3, id);
  }

  @Override
  protected Avatar sqlToEntity(ResultSet rs) throws SQLException {
    Integer id = rs.getInt("id");
    String name = rs.getString("name");
    String image = rs.getString("image");
    Integer cost = rs.getInt("cost");
    String description = rs.getString("description");
    return new Avatar(id, name, image, cost, description);
  }

  public Avatar getByName(String name) {
    Avatar avatar;
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByNameSql);
      stmt.setString(1, name);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        avatar = sqlToEntity(rs);
        return avatar;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public Avatar getByCost(Integer cost) {
    Avatar avatar;
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getByCostSql);
      stmt.setInt(1, cost);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        avatar = sqlToEntity(rs);
        return avatar;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public List<Avatar> getAvatarsByUserId(String userId) {
    List<Avatar> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(this.getAvatarsByUserIdSQL);
      stmt.setString(1, userId);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        list.add(sqlToEntity(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }
}
