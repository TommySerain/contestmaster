package co.contestmaster.repository.interfaces;

import co.contestmaster.entity.Party;
import java.util.List;

public interface PartyRepositoryInterface extends CrudRepository<Party> {
  List<Party> getByRoundByTournament(String round, int tournamentId);

  List<Party> getPartiesByTournament(int id);
}
